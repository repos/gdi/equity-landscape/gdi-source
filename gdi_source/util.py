from typing import Generator, List
import fsspec
from workflow_utils.util import fsspec_use_new_pyarrow_api

import re
import luadata
from pyspark.sql import DataFrame, SparkSession
import pyspark.sql.functions as F
from pyspark.sql.types import StructType
from pyspark.sql.utils import AnalysisException

import wbgapi as wb

from mediawiki import MediaWiki

"""
This module contains utility functions for the Equity Landscape project. It provides functionalities to fetch data from the World Bank Data API and MediaWiki, process and transform the data, and store it in Hive or CSV files.

Functions:
    set_proxy(proxy_dict: dict = proxies) -> None:
        Sets the proxy for the World Bank API.
    fetch_world_bank_data(db: int, series: str) -> Generator:
        Fetches data from the World Bank API.
    fetch_lua_data(page_name: str) -> List[dict]:
        Fetches lua table page using the MediaWiki API and parses the data into a list of dicts.
    rename_columns(df: DataFrame, schema: StructType) -> DataFrame:
        Renames the dataframe columns to match the provided schema.
    handle_data_types(df: DataFrame, date_format: str) -> DataFrame:
        Handles data types for the dataframe.
    convert_data_types(df: DataFrame, schema: StructType, date_format: str = "YYYY-mm-dd") -> DataFrame:
        Converts data types for the dataframe based on the schema.
    handle_partitions(df: DataFrame, partition_columns: str) -> DataFrame:
        Handles partition columns for the dataframe.
    get_columns(df: DataFrame, exclude_cols: list = []) -> list:
        Returns a list of columns in a dataframe excluding the exclude_cols.
    create_select(columns: List[str]) -> str:
        Returns a string of columns to be used in a select statement.
    get_schema(spark_sql: SparkSession, table_name: str) -> StructType:
        Returns the schema of a table.
    get_partition_statement(partition_columns: str) -> str:
        Returns a partition statement for a given dataframe and partition column.
    get_partition_columns(table_name: str, spark: SparkSession) -> List[str]:
        Returns the partition columns for a given table.
    drop_partition_columns(df: DataFrame, partition_columns: str) -> DataFrame:
        Drops partition columns from the dataframe.
    remove_partition_column_from_schema(schema: StructType, partition_columns: List[str]) -> StructType:
        Removes partition columns from a given schema.
    write_results(year: int, spark: SparkSession, table_name: str, df: DataFrame) -> None:
        Writes the results to a table.
    df_to_list_of_dicts(df: DataFrame) -> list:
        Converts a dataframe to a list of dictionaries.
    insert_into_csv_table(table_name: str, partition_statement: str, df: DataFrame, spark: SparkSession) -> None:
        Inserts data into a csv table.
    handle_true_false(field_name: str,value: str) -> bool:
        Handles true/false values.
    filter_out_metrics(df: DataFrame, join_column: str, lst_blacklist_countries: list = None, lst_exclude_metrics: list = None) -> DataFrame:
        Filters out metrics from a dataframe based on a blacklist of countries and metrics.
    write_df_to_json(df: DataFrame, filename: str):
        Writes a dataframe to a json file.
    is_valid_sql_name(name: str):
        Checks if a string is a valid SQL name.
    format_string(input_string: str, kwargs: dict):
        Replaces placeholders in a string with values from a dictionary. Used for spark queries.
    _is_empty_string(value: str):
        Checks if a string is empty.
    _is_empty_list(value: list):
        Checks if a list is empty.
    _is_empty_dict(value: dict):
        Checks if a dictionary is empty.
    _is_empty_dataframe(value: DataFrame):
        Checks if a dataframe is empty.
    _is_empty_schema(value: StructType):
        Checks if a schema is empty.
"""


URL = "http://webproxy.eqiad.wmnet:8080"
proxies = {
    "http": URL,
    "https": URL,
}

NULL_DF_ERROR = "df cannot be None"
NULL_SCHEMA_ERROR = "schema cannot be None"

fsspec_use_new_pyarrow_api(should_set_hadoop_env_vars=True)




# takes in a proxy dict to allow http calls to be made from airflow
def set_proxy(proxy_dict: dict = proxies) -> None:
    """
    param dict proxy_dict: Dictionary representing the proxy details to be used
    """
    if _is_empty_dict(proxy_dict):
        raise ValueError("proxy_dict cannot be empty")

    wb.proxies = proxy_dict


# fetch data from World Bank API and returns a generator You might get timeouts if the proxy is not set, so be sure
# to set the proxy first before calling fetch_data unless if calling on your local machine.
def fetch_world_bank_data(db: int, series: str) -> Generator:
    """
    param int db: number denoting an indicator in the WorldBank database
    param str series: Series name used to denote KPI in the WorldBank database
    returns a Generator

    kpi_name	                                        series_identifier	db_id
    Control of Corruption	                            CC.EST	            3
    Mobile cellular subscriptions (per 100 people)	    IT.CEL.SETS.P2	    2
    Individuals using the Internet (% of population)	IT.NET.USER.ZS	    2
    Population	                                        SP.POP.TOTL	        2
    GDP, PPP (current international $)	                NY.GDP.MKTP.PP.CD	2
    GDP, PPP (constant 2017 international $)	        NY.GDP.MKTP.PP.KD	2
    World Bank Gender Ratios	                        SP.POP.TOTL.FE.ZS	2
    """
    if (db is None) or _is_empty_string(series):
        raise ValueError(f"db and series cannot be None - db = {db} series = {series}")
    else:
        try:
            fetched_data = wb.data.fetch(series=series, db=db)
            yield from fetched_data
        except Exception as e:
            raise ValueError(f"db = {db} series = {series} - Error fetching data from World Bank API: {e}")


# Fetch lua table page using the mediawiki api and parse the data into a list of dicts
def fetch_lua_data(page_name: str) -> List[dict]:
    """
    param str page_name: name of the lua table page
    returns list of dicts
    """
    if _is_empty_string(page_name):
        raise ValueError("page_name cannot be None or empty")

    mw = MediaWiki(lang="meta")
    page = mw.page(page_name)
    return luadata.unserialize(page.wikitext, encoding="utf-8", multival=False)


def rename_columns(df: DataFrame, schema: StructType) -> DataFrame:
    """Renames columns to match the schema"""
    if _is_empty_schema(schema):
        raise ValueError(NULL_SCHEMA_ERROR)

    if _is_empty_dataframe(df):
        raise ValueError(NULL_DF_ERROR)

    return df.toDF(*[field.name for field in schema.fields])


def handle_data_types(df: DataFrame, date_format: str) -> DataFrame:
    """Handles data types for the dataframe"""
    if _is_empty_dataframe(df):
        raise ValueError(NULL_DF_ERROR)

    for column_name, column_type in df.dtypes:
        df = _handle_data_type(df, date_format, column_name, column_type)

    return df


def _handle_data_type(df: DataFrame, date_format: str, column_name: str, column_type: str) -> DataFrame:
    if "array" in column_type:
        df = df.withColumn(column_name, F.split(column_name, ","))

    elif "date" in column_type:
        df = df.withColumn(
            column_name, F.expr(f"to_date({column_name},'{date_format}')")
        )

    elif _is_numeric(column_type):
        df = df.withColumn(column_name, F.col(column_name).cast(column_type))
    return df


def _is_numeric(column_type: str) -> bool:
    """Checks if a column type is numeric"""
    numeric_types = {"decimal", "int", "long", "float", "double", "bigint", "short", "byte", "integer"}
    return column_type in numeric_types



def convert_data_types(
    df: DataFrame, schema: StructType, date_format: str = "YYYY-mm-dd"
) -> DataFrame:
    """Converts data types for the dataframe based on the schema"""

    # rename columns to match the schema

    if _is_empty_dataframe(df):
        raise ValueError(NULL_DF_ERROR)

    if schema is None:
        raise ValueError(NULL_SCHEMA_ERROR)

    if _is_empty_string(date_format):
        raise ValueError("date_format cannot be None or empty")

    df = rename_columns(df, schema)

    for schema in schema.fields:

        column_name, column_type = schema.simpleString().split(":")

        df = _handle_data_type(df, date_format, column_name, column_type)

    return df

def handle_partitions(df: DataFrame, partition_columns: str) -> DataFrame:
    """
    Returns a dataframe with the partition column properly formatted.

    Args:
        df (DataFrame): The input dataframe.
        partition_columns (str): A string containing the partition columns and their values.

    Returns:
        DataFrame: The input dataframe with the partition columns dropped.

    """
    # If partition_columns is not None or empty
    if partition_columns is not None and partition_columns != "":
        # Split the partition_columns string into a list of partitions
        partitions = partition_columns.split(",")
        # For each partition
        for partition in partitions:
            # Get the partition column name
            partition_column = partition.split("=")[0].strip()
            # Drop the partition column from the dataframe
            df = df.drop(partition_column)

    # Return the (un)modified dataframe with the partition columns dropped if any
    return df

def get_columns(df: DataFrame, exclude_cols: list = []) -> list:
    """
    Returns a list of columns in a dataframe excluding the exclude_cols

    Args:
        df (DataFrame): The input dataframe.
        exclude_cols (list): A list of columns to exclude.
    """
    if _is_empty_dataframe(df):
        raise ValueError(NULL_DF_ERROR)

    if exclude_cols is None:  # create an empty list if exclude_cols is None instead of raising an error
        exclude_cols = []

    return [x for x in df.columns if x not in exclude_cols]


def create_select(columns: List[str], prefix: str = None) -> str:
    """
    Returns a string of columns to be used in a select statement.

    Args:
        columns (List[str]): A list of columns to be selected.

    Returns:
        str: A string of columns to be used in a select statement.

    """
    # If columns is an empty list, return an empty string
    if not columns:
        return ""

    # Initialize an empty string for the select statement
    select = ""

    # For each column in the list of columns
    for col in columns:

        # If a prefix is provided, add it to the column name
        column_name = f"{prefix}.{col}" if prefix else col

        # Add the column to the select statement with a comma separator
        select += f"{column_name},"

    # Remove the trailing comma from the select statement and return it
    return select[:-1]


def get_schema(spark_sql: SparkSession, table_name: str) -> StructType:
    """
    Returns the schema of a table.

    Args:
        spark_sql (SparkSession): The SparkSession object.
        table_name (str): The name of the table.

    Returns:
        StructType: The schema of the table.

    Raises:
        ValueError: If table_name is None or empty.

    """
    # If table_name is None or empty, raise a ValueError
    if _is_empty_string(table_name):
        raise ValueError("table_name cannot be None or empty")

    # Return the schema of the table
    return spark_sql.table(table_name).schema


def get_partition_statement(partition_columns: str) -> str:
    """
    Returns a partition statement for a given dataframe and partition column.

    Args:
        partition_columns (str): A string containing the partition columns.

    Returns:
        str: A partition statement for the given dataframe and partition column.

    """
    # Construct a partition statement with the given partition columns
    return (
        "PARTITION ({})".format(partition_columns)
        if not _is_empty_string(partition_columns)
        else ""
    )



def get_partition_columns(table_name: str, spark: SparkSession) -> List[str]:
    """Returns the partition columns for a given table

    This is a hacky way to get the partition columns and as such is an expensive operation.

    Args:
        table_name (str): The name of the table.
        spark (SparkSession): The SparkSession object.

    Returns:
        List[str]: A list of partition columns for the given table.

    Raises:
        ValueError: If table_name is None or empty.

    """
    if _is_empty_string(table_name):
        raise ValueError("table_name cannot be None or empty")

    try:
        # Get the data type of the partition columns for the given table using describe.
        row_columns = spark.sql(f"describe {table_name}").where("col_name like 'Part%'").select("data_type").collect()
        # Extract the data type from each row and return as a list
        return [row.data_type for row in row_columns]
    except AnalysisException:
        # When there are no partitions or the table does not exist, return an empty list
        return []


def drop_partition_columns(df: DataFrame, partition_columns: str) -> DataFrame:
    """Returns a dataframe with the partition column properly formatted"""
    if partition_columns is not None and partition_columns != "":
        partitions = partition_columns.split(",")
        for partition in partitions:
            partition_column = partition.split("=")[0].strip()
            df = df.drop(partition_column)

    return df


def remove_partition_column_from_schema(schema: StructType, partition_columns: List[str]) -> StructType:
    """Removes partition columns from a given schema.

    Args:
        schema (StructType): The schema to remove partition columns from.
        partition_columns (List[str]): A list of partition columns to remove.

    Returns:
        StructType: The updated schema with partition columns removed.

    Raises:
        ValueError: If schema or partition_columns is None.
        ValueError: If schema is empty.
    """
    if schema is None or partition_columns is None:
        raise ValueError("schema and partition_columns cannot be None")

    if len(schema) == 0:
        raise ValueError("schema cannot be empty")

    # Create a new list of StructFields without the partition columns
    new_fields = [field for field in schema.fields if field.name not in partition_columns]

    # Return a new StructType with the updated list of StructFields
    return StructType(new_fields)


def write_results(
    year: int,
    spark: SparkSession,
    table_name: str,
    df: DataFrame,
) -> None:
    """Writes the results to a table.

    Args:
        year (int): The year for the data to be used as a partition.
        spark (SparkSession): The SparkSession object.
        table_name (str): The name of the table.
        df (DataFrame): The dataframe to write.

    Returns:
        None

    Raises:
        ValueError: If the dataframe is empty or None.
        ValueError: If the table name is None or empty.
        ValueError: If the table name is not a valid SQL name.
    """

    # Check if there is data to write
    if df is None or df.count() == 0:
        raise ValueError("No data to write")

    # Check if a table name is provided and if it is a valid SQL name
    if _is_empty_string(table_name):
        raise ValueError("No table name provided")
    elif not is_valid_sql_name(table_name):
        raise ValueError(f"Invalid table name: {table_name}")

    # Get the schema of the table
    table_schema = get_schema(spark_sql=spark, table_name=table_name)

    # Fill null values with 0
    df = df.fillna(0)

    # Create partition columns
    partition_columns = f"year='{year}'"

    # Get the partition statement
    partition_statement = get_partition_statement(partition_columns)

    # Drop partition columns from the dataframe
    df = drop_partition_columns(df=df, partition_columns=partition_columns)

    # Remove partition columns from the table schema
    table_schema = remove_partition_column_from_schema(table_schema, partition_columns)

    # Convert data types of the dataframe to match the table schema
    df = convert_data_types(df, table_schema)

    # Create a temporary view of the dataframe
    df.createOrReplaceTempView("output_table")

    # Insert the data into the table
    spark.sql(
        f"""
      INSERT OVERWRITE TABLE {table_name} {partition_statement}
      SELECT *
        FROM output_table
    """
    )



def df_to_list_of_dicts(df: DataFrame) -> list:
    """
    df: dataframe to be converted to a list of dicts

    """
    return [row.asDict() for row in df.collect()]

def insert_into_csv_table(table_name: str, partition_statement: str, df: DataFrame, spark: SparkSession) -> None:
    """Inserts data into a csv table.

    Args:
        table_name (str): The name of the table.
        partition_statement (str): The partition statement.
        df (DataFrame): The dataframe to write.
        spark (SparkSession): The SparkSession object.

    Returns:
        None

    Raises:
        ValueError: If the dataframe is empty or None.
        ValueError: If the table name is None or empty.

    """

    # Check if a table name is provided
    if _is_empty_string(table_name):
        raise ValueError("No table name provided")

    # Check if the dataframe is empty
    if _is_empty_dataframe(df):
        raise ValueError(NULL_DF_ERROR)

    # Create a temporary view of the dataframe
    df.createOrReplaceTempView("csv_table")

    # Insert the data into the CSV table
    spark.sql(
        f"""
      INSERT OVERWRITE TABLE {table_name} {partition_statement}
      SELECT *
        FROM csv_table
    """
    )

def handle_true_false(field_name: str,value: str) -> bool:
    """Handles true/false values"""
    field_value = int(value)
    if field_value == 1:
        return True
    elif field_value == 0:
        return False
    else:
        raise ValueError(f"Error --{field_name} only supports values 1 (True) or 0 (False)")



def filter_out_metrics(
    df: DataFrame,
    join_column: str,
    lst_blacklist_countries: list = None,
    lst_exclude_metrics: list = None,
) -> DataFrame:

    # check if dataframe is empty or join_column is None then raise an error
    if _is_empty_string(join_column) or df is None:
        raise ValueError("Dataframe or join_column cannot be None")

    # if lst_blacklist_countries is not None or lst_exclude_metrics is not None then return the dataframe without the metrics filtered.
    if lst_blacklist_countries is None or lst_exclude_metrics is None:
        return df

    # Get the column types of the dataframe to exclude columns that are not numeric
    column_types = dict(df.dtypes)

    # Filter out columns that are not numeric and exclude the year column
    lst_filtered_columns = [
        column_name
        for column_name, column_type in column_types.items()
        if _is_numeric(column_type)
        and column_name != "year"

        and column_name not in lst_exclude_metrics
    ]

    # Get all the columns in the dataframe
    all_columns = df.columns

    # Filter out the columns that are in the blacklist countries list and set the values to 0
    for column in lst_filtered_columns:
        if column in all_columns:
            df = df.withColumn(
                column,
                F.when(df[join_column].isin(lst_blacklist_countries), 0).otherwise(
                    df[column]
                ),
            )

    return df



def write_df_to_json(df: DataFrame, filename: str):
    """
    df: dataframe to be written to a json file
    filename: a valid path to the file
    """
    if df is None:
        raise ValueError("df cannot be None")
    if _is_empty_string(filename):
        raise ValueError("filename cannot be None or empty")

    records = df.toPandas().to_json(orient="records")

    if records is None:
        raise ValueError("records cannot be None")

    with fsspec.open(filename, "w") as f:
        f.write(records)


def is_valid_sql_name(name: str):
    if _is_empty_string(name):
        return False
    return re.match(r"^[\w\.]+$", name) is not None

def format_string(input_string, **kwargs):
    return input_string.format(**kwargs)

def _is_empty_string(value: str):
    return value is None or value == ""

def _is_empty_list(value: list):
    return value is None or len(value) == 0

def _is_empty_dict(value: dict):
    return value is None or len(value) == 0

def _is_empty_dataframe(value: DataFrame):
    return value is None or value.count() == 0

def _is_empty_schema(value: StructType):
    return value is None or len(value) == 0




