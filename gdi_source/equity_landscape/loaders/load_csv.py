import argparse

import pyspark.sql.types as T
from pyspark import SparkConf
from pyspark.sql import SparkSession
from pyspark.sql import functions as F

from gdi_source.util import (convert_data_types, get_partition_columns,
                             get_partition_statement, get_schema,
                             handle_partitions, handle_true_false,
                             insert_into_csv_table)

"""
This module contains functions to load a CSV file from a specified path and write it to an Iceberg/Hive table.
The module contains the following functions:
    - _process_args(): Processes command line arguments
    - _main(): Loads csv file from a specified path and writes it to Hive or Iceberg
"""


def _cast_partition_value(partition_value, partition_column, table_schema):
    column_data_type = table_schema[partition_column].dataType

    if isinstance(column_data_type, T.StringType):
        return str(partition_value)
    elif isinstance(column_data_type, (T.IntegerType, T.LongType)):
        if isinstance(partition_value, int):
            return partition_value
        else:
            return int(partition_value.replace("'", ""))
    elif isinstance(column_data_type, T.DoubleType):
        if isinstance(partition_value, str):
            partition_value = partition_value.replace("'", "")
        return float(partition_value)
    else:
        return partition_value

def _process_args():
    """Processes command line arguments"""

    # Create an ArgumentParser object
    parser = argparse.ArgumentParser()

    # Add the command line arguments to the parser
    parser.add_argument(
        "--table_name", help="Name of the table to be written", required=True
    )
    parser.add_argument("--file_location", help="CSV file location", required=True)
    parser.add_argument(
        "--headers",
        help="True/False whether there are headers on the file (denoted by 1 (True) / 0 (False))",
        required=False,
    )
    parser.add_argument(
        "--partition_columns",
        help="A string representation of the partitions to use",
        required=False,
    )
    parser.add_argument(
        "--date_format", help="Date format to be used", required=False
    )
    parser.add_argument(
        "--iceberg",
        help="True or False if this is an iceberg table denoted by 1 (True) or 0 (False)",
        required=False,
    )

    # Parse the command line arguments
    args = parser.parse_args()

    # Return the parsed arguments
    return args


def _main(
    table_name: str,
    file_location: str,
    headers: bool,
    is_iceberg: bool,
    partition_columns: str = None,
    date_format: str = None
) -> None:
    """
    Loads csv file from a specified path and writes it to Hive

    param table_name: Name of the table to be written
    param file_location: CSV file location
    param headers: True/False whether there are headers on the file (denoted by 1 (True) / 0 (False))
    param partition_columns: A string representation of the partitions to use
    param date_format: Date format to be used

    This is meant to work for both iceberg and non-iceberg tables

    e.g /path/to/load_csv.py --table_name gdi.social_progress_metrics --file_location /path/to/csv/social_progress.csv --partition_columns year='2021' --headers 0 --date_format 'yyyy-MM-dd'
    """

    conf = SparkConf().setAppName("load_csv")

    spark = SparkSession.builder.config(conf=conf).getOrCreate()

    # Set the time parser policy to LEGACY to avoid errors when parsing dates
    spark.sql("set spark.sql.legacy.timeParserPolicy=LEGACY")

    df = spark.table(table_name)

    # Get the schema of the table with the given name
    # The schema is used to convert the data types of the DataFrame to match the table
    table_schema = get_schema(spark_sql=spark, table_name=table_name)

    # Read the CSV file from the given location into a DataFrame
    df_csv = spark.read.option("sep", "\t").option("header", headers).option("sep", ";").option("sep", ",").option("inferSchema", True).csv(file_location)

    # If the date format is not specified, use the default "yyyy-MM-dd"
    if date_format is None:
        date_format = "yyyy-MM-dd"

    # Initialize the list of partition columns to an empty list
    # This list is used to sort the DataFrame within partitions when writing to Iceberg
    # If the target table is not Iceberg, this list is not used
    lst_partition_columns = []

    # If partition columns are specified and the target table is Iceberg
    if partition_columns is not None and is_iceberg:
        # Split the partition columns string into a list of partitions
        partitions = partition_columns.split(",")
        # For each partition
        for partition in partitions:
            # Split the partition into column and value
            partition_split = partition.split("=")
            partition_value = partition_split[1]
            partition_column = partition_split[0]

            # Add the partition column to the list of partition columns
            lst_partition_columns.append(partition_column)


            # Cast the partition value to the data type of the partition column
            casted_value = _cast_partition_value(partition_value, partition_column, table_schema)

            # Cast the partition value to the data type of the partition column and add it to the DataFrame
            df_csv = df_csv.withColumn(
                partition_column, F.lit(casted_value).cast(table_schema[partition_column].dataType)
            )
    # If partition columns are specified and the target table is not Iceberg
    elif not is_iceberg:
        # Get the partition statement for the given partition columns
        partition_statement = get_partition_statement(partition_columns=partition_columns)
        # Handle the partitions in the DataFrame
        df = handle_partitions(df=df, partition_columns=partition_columns)

        # Use the schema of the table to convert the data types of the DataFrame columns to match the table schema
        table_schema = df.schema

    # Convert the data types of the DataFrame columns to match the table schema, and drop duplicates
    df_csv = convert_data_types(df=df_csv, schema=table_schema, date_format=date_format).drop_duplicates()

    # If the target table is Iceberg
    if is_iceberg:
        # If the list of partition columns is not empty, sort the DataFrame within partitions
        lst_partition_columns = lst_partition_columns if lst_partition_columns else get_partition_columns(spark=spark, table_name=table_name)
        if lst_partition_columns:
            df_csv = df_csv.sortWithinPartitions(lst_partition_columns)
        # Write the DataFrame to the target table, overwriting existing partitions
        df_csv.writeTo(table_name).overwritePartitions()
    # If the target table is not Iceberg
    else:
        # Insert the DataFrame into the target CSV table, using the given partition statement
        insert_into_csv_table(df=df_csv, table_name=table_name, partition_statement=partition_statement, spark=spark)

    # Stop the SparkSession
    spark.stop()




def run():
    """Validates command line arguments and runs the main function"""

    # Parse the command line arguments
    args = _process_args()

    # Get the table name from the command line arguments
    if args.table_name:
        table_name = args.table_name
    else:
        raise ValueError("table_name not specified")

    # Get the file location from the command line arguments
    if args.file_location:
        file_location = args.file_location
    else:
        raise ValueError("file_location not specified")

    # Get the headers flag from the command line arguments, or assume True if not specified
    headers = handle_true_false(field_name="headers", value=args.headers) if args.headers else True

    # Get the partition columns from the command line arguments, or assume None if not specified
    partition_columns = args.partition_columns if args.partition_columns else None

    # Get the date format from the command line arguments, or assume None if not specified
    date_format = args.date_format if args.date_format else None

    # Get the Iceberg flag from the command line arguments, or assume False if not specified
    iceberg = handle_true_false(field_name="iceberg", value=args.iceberg) if args.iceberg else False

    # Call the main function with the parsed arguments
    _main(table_name=table_name,
          file_location=file_location,
          headers=headers,
          is_iceberg=iceberg,
          partition_columns=partition_columns,
          date_format=date_format
          )


if __name__ == "__main__":
    run()
