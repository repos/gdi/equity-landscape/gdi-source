import argparse
from datetime import datetime
from typing import List

import pyspark.sql.functions as F
from dateutil.parser import parse
from pyspark import SparkConf
from pyspark.sql import SparkSession
from pyspark.sql.types import (ArrayType, DateType, DoubleType, IntegerType,
                               StringType, StructField, StructType)

from gdi_source.util import fetch_lua_data


def _handle_args():
    """Handles command line arguments."""
    parser = argparse.ArgumentParser()
    parser.add_argument("--current_year", type=int, required=True)
    parser.add_argument("--end_year", type=int, required=True)
    parser.add_argument("--schema", type=str, required=True)
    return parser.parse_args()


def _get_schema() -> StructType:
    """
    Returns the schema for the organizational_info_input_metrics table
    """
    return StructType(
        [
            StructField("unique_id", StringType(), True),
            StructField("affiliate_code", StringType(), True),
            StructField("group_name", StringType(), True),
            StructField("org_type", StringType(), True),
            StructField("region", StringType(), True),
            StructField("group_country", StringType(), True),
            StructField("legal_entity", StringType(), True),
            StructField("mission_changed", StringType(), True),
            StructField("group_page", StringType(), True),
            StructField("member_count", IntegerType(), True),
            StructField("non_editors_count", IntegerType(), True),
            StructField("affiliate_size", IntegerType(), True),
            StructField("facebook", StringType(), True),
            StructField("twitter", StringType(), True),
            StructField("other", StringType(), True),
            StructField("dm_structure", ArrayType(StringType(), False), True),
            StructField("group_contact1", StringType(), True),
            StructField("group_contact2", StringType(), True),
            StructField("board_contacts", ArrayType(StringType(), False), True),
            StructField("agreement_date", StringType(), True),
            StructField("fiscal_year_start", StringType(), True),
            StructField("fiscal_year_end", StringType(), True),
            StructField("me_bypass_ooc_autochecks", StringType(), True),
            StructField("out_of_compliance_level", IntegerType(), True),
            StructField("reporting_due_date", DateType(), True),
            StructField("uptodate_reporting", StringType(), True),
            StructField("notes_on_reporting", StringType(), True),
            StructField("recognition_status", StringType(), True),
            StructField("derecognition_date", StringType(), True),
            StructField("derecognition_notes", StringType(), True),
            StructField("dos_stamp", StringType(), True),
            StructField("affiliate_tenure", DoubleType(), True),
        ]
    )


def run():
    args = _handle_args()
    _main(
        current_year=args.current_year,
        end_year=args.end_year,
        schema=args.schema,
    )


def _main(
    current_year: int,
    end_year: int,
    schema: str,
) -> None:
    """
    Fetches lua_data from MediaWiki and processes it to create a dataframe and store it on Hive.

    param str page_name: name of the lua table page
    param int current_year: current year
    param int end_year: end year
    param str schema: schema name

    e.g.  /path/to/lua_data.py --current_year 2021 --end_year 2022 --schema gdi

    """

    org_schema = _get_schema()

    conf = SparkConf()
    conf.setAppName("lua_data")

    spark = SparkSession.builder.config(conf=conf).getOrCreate()

    lst_lua_data_raw = fetch_lua_data(page_name="Module:Organizational_Informations")

    if len(lst_lua_data_raw) > 0:
        lst_lua_data = process(lst_lua_data_raw)
        df_org_info = spark.createDataFrame(lst_lua_data, schema=org_schema)

        affiliate_size_calc = F.expr(
            """
            CASE
                WHEN member_count < 10 THEN
                    1
                WHEN member_count < 29 THEN
                    2
                WHEN member_count < 50 THEN
                    3
                WHEN member_count < 100 THEN
                    4
                WHEN member_count > 100 THEN
                    5
            END
        """
        )

        affiliate_tenure_calc = F.expr(
            """
        datediff(to_date('{end_year}/12/31','yyyy/MM/dd'),to_date(agreement_date,'dd/MM/yyyy'))/365.25
        """
        )

        df_org_info = df_org_info.withColumn("affiliate_size", affiliate_size_calc)

        df_org_info = df_org_info.withColumn("affiliate_tenure", affiliate_tenure_calc)

        df_org_info.createOrReplaceTempView("org_info")

        spark.sql(
            f"""
            INSERT OVERWRITE TABLE {schema}.organizational_info_input_metrics PARTITION(year={current_year})
            SELECT *
                FROM org_info
            """
        )


def _process_board_contacts(value: dict) -> List[str]:
    """
    Processes the board contacts field to return a list of contacts
    """
    board_string = value.get("board_contacts", None)

    if board_string is not None:
        board_array = board_string.split("<br />")
        board_array = [x.strip() for x in board_array]
        return board_array if len(board_array) > 0 else None


def _process_reporting_due_date(value: dict) -> datetime:
    """
    Processes the reporting_due_date field to return a date
    """
    reporting_due_date = value.get("reporting_due_date", None)
    if reporting_due_date is not None and reporting_due_date != "":
        try:
            return parse(reporting_due_date)
        except ValueError:
            pass


def _process_int(value: dict, field: str) -> int:
    """
    Processes the field to return an int
    """
    field_value = value.get(field, None)
    if field_value is not None and field_value != "":
        try:
            return int(field_value)
        except ValueError:
            pass


def process(lst_lua_table_raw: List[dict]) -> List[dict]:
    """
    param list lst_lua_table: list of dicts representing the lua table
    returns list of dicts
    """
    for index, value in enumerate(lst_lua_table_raw):

        lst_lua_table_raw[index]["board_contacts"] = _process_board_contacts(value)

        dm_structure = value.get("dm_structure", None)

        if len(dm_structure) == 0:
            lst_lua_table_raw[index]["dm_structure"] = None

        lst_lua_table_raw[index]["reporting_due_date"] = _process_reporting_due_date(
            value
        )

        lst_lua_table_raw[index]["member_count"] = _process_int(value, "member_count")

        lst_lua_table_raw[index]["non_editors_count"] = _process_int(
            value, "non_editors_count"
        )

        lst_lua_table_raw[index]["out_of_compliance_level"] = _process_int(
            value, "out_of_compliance_level"
        )

    return lst_lua_table_raw


if __name__ == "__main__":
    run()
