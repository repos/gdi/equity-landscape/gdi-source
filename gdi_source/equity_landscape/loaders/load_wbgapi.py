import argparse
from typing import List

from pyspark import SparkConf
from pyspark.sql import DataFrame, SparkSession
from pyspark.sql.types import (BooleanType, DoubleType, StringType,
                               StructField, StructType)

from gdi_source.util import fetch_world_bank_data, set_proxy

"""
This module contains functions to fetch data from the World Bank Data API, process it, and store it in Hive.

Functions:
    _handle_args() -> argparse.Namespace:
        Parses command line arguments and returns an argparse.Namespace object.
    run() -> None:
        Parses command line arguments and calls _main() with the parsed arguments.
    _main(db: int, series: str, schema: str, is_iceberg: bool) -> None:
        Fetches data from the World Bank Data API, processes it, creates a dataframe, and stores it in Hive.
    _process(lst_results: List[dict]) -> List[dict]:
        Processes the results of the World Bank Data API query and returns a list of dictionaries.
"""


def _handle_args():
    """Handle command line arguments."""
    parser = argparse.ArgumentParser()
    parser.add_argument("--db", type=int, required=True)
    parser.add_argument("--series", type=str, required=True)
    parser.add_argument("--schema", type=str, required=True)
    parser.add_argument("--is-iceberg", type=bool, required=False, default=False)
    return parser.parse_args()


def run():
    args = _handle_args()
    _main(db=args.db, series=args.series, schema=args.schema, is_iceberg=args.is_iceberg)


def _main(db: int, series: str, schema: str, is_iceberg: bool) -> None:
    """
    Fetches data from the world bank data api and processes it to create a dataframe and store it on Hive.

    param int db: world bank database id
    param str series: world bank series id
    param str schema: schema name

    e.g.  /path/to/load_wbgapi.py --db 2 --series IT.NET.USER.ZS --schema gdi

    """
    wb_schema = StructType(
        [
            StructField("value", DoubleType(), True),
            StructField("series", StringType(), True),
            StructField("economy", StringType(), True),
            StructField("aggregate", BooleanType(), True),
            StructField("time", StringType(), True),
        ]
    )

    set_proxy()

    conf = SparkConf()
    conf.setAppName("load_wbgapi")
    spark = SparkSession.builder.config(conf=conf).getOrCreate()

    results = fetch_world_bank_data(db=db, series=series)

    lst_results = list(results)

    if len(lst_results) > 0:

        lst_results = _process(lst_results)

        df_wb_data: DataFrame  = spark.createDataFrame(lst_results, schema=wb_schema)

        table_name = f"{schema}.world_bank_data_input_metrics"

        if is_iceberg:
            df_wb_data.writeTo(table_name).overwritePartitions()
        else:

            df_wb_data = df_wb_data.drop(
                "series"
            )  # drop series column as it is a partition column

            df_wb_data.createOrReplaceTempView("wb_data")

            spark.sql(
                f"""
                INSERT OVERWRITE TABLE {schema}.world_bank_data_input_metrics PARTITION (series='{series}')
                SELECT *
                    FROM wb_data
                """
            )
    else:
        raise ValueError(f"wbgapi for DB:{db} and Series: {series} returned 0 results")


def _process(lst_results: List[dict]) -> List[dict]:
    """
    param list lst_results
    returns list of dicts
    """
    for index, value in enumerate(lst_results):
        wb_value = value.get("value", None)

        if wb_value == "" or wb_value is None:
            wb_value = None
        else:
            wb_value = float(wb_value)

        lst_results[index]["value"] = wb_value

    return lst_results


if __name__ == "__main__":
    run()
