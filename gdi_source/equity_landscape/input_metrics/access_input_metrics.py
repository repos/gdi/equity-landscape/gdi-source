import argparse

from pyspark import SparkConf
from pyspark.sql import SparkSession
from pyspark.sql.types import (DoubleType, IntegerType, StringType,
                               StructField, StructType)

import gdi_source.util as Utils

"""
This module contains functions to calculate Access input metrics and store them in Hive/Iceberg.

Functions:
    _handle_args() -> argparse.Namespace:
        Parses command line arguments and returns an argparse.Namespace object.
    _get_mci_schema() -> StructType:
        Returns the schema for the Mobile Connectivity Index (MCI) data.
    _main(db: int, series: str, schema: str, is_iceberg: bool) -> None:
        Uses data stored in Hive/Iceberg to calculate Access input metrics and store them in Hive/Iceberg.
    run() -> None:
        Parses command line arguments and calls _main() with the parsed arguments.
"""


def _get_mci_schema() -> StructType:
    return StructType(
        [
            StructField("iso_code", StringType(), True),
            StructField("country_name", StringType(), True),
            StructField("region", StringType(), True),
            StructField("score_year", IntegerType(), True),
            StructField("cluster", StringType(), True),
            StructField("connectivity_index", DoubleType(), True),
            StructField("infrastructure", DoubleType(), True),
            StructField("affordability", DoubleType(), True),
            StructField("consumer_readiness", DoubleType(), True),
            StructField("content_and_services", DoubleType(), True),
            StructField("network_coverage", DoubleType(), True),
            StructField("network_performance", DoubleType(), True),
            StructField("other_enabling_infrastructure", DoubleType(), True),
            StructField("spectrum", DoubleType(), True),
            StructField("mobile_tariffs", DoubleType(), True),
            StructField("handset_prices", DoubleType(), True),
            StructField("taxation", DoubleType(), True),
            StructField("inequality", DoubleType(), True),
            StructField("mobile_ownership", DoubleType(), True),
            StructField("basic_skills", DoubleType(), True),
            StructField("gender_equality", DoubleType(), True),
            StructField("local_relevance", DoubleType(), True),
            StructField("availability", DoubleType(), True),
            StructField("online_security", DoubleType(), True),
            StructField("coverage_2g", DoubleType(), True),
            StructField("coverage_3g", DoubleType(), True),
            StructField("coverage_4g", DoubleType(), True),
            StructField("coverage_5g", DoubleType(), True),
            StructField("mobile_download_speeds", DoubleType(), True),
            StructField("mobile_upload_speeds", DoubleType(), True),
            StructField("mobile_latencies", DoubleType(), True),
            StructField("access_to_electricity", DoubleType(), True),
            StructField("servers_per_population", DoubleType(), True),
            StructField(
                "international_internet_bandwidth_per_user", DoubleType(), True
            ),
            StructField("ixps_per_population", DoubleType(), True),
            StructField("digital_dividend_spectrum", DoubleType(), True),
            StructField("other_spectrum_below_1ghz", DoubleType(), True),
            StructField("spectrum_in_1_3ghz_bands", DoubleType(), True),
            StructField("spectrum_above_3ghz_bands", DoubleType(), True),
            StructField("spectrum_in_mmwave_bands", DoubleType(), True),
            StructField("entry_basket", DoubleType(), True),
            StructField("medium_basket", DoubleType(), True),
            StructField("high_basket", DoubleType(), True),
            StructField("premium_basket", DoubleType(), True),
            StructField("device_price", DoubleType(), True),
            StructField("tax_as_a_perc_of_tcmo", DoubleType(), True),
            StructField("mobile_specific_taxes_as_perc_of_tcmo", DoubleType(), True),
            StructField("literacy", DoubleType(), True),
            StructField("school_life_expectancy", DoubleType(), True),
            StructField("mean_years_of_schooling", DoubleType(), True),
            StructField("tertiary_enrolment", DoubleType(), True),
            StructField("gender_parity_in_schooling", DoubleType(), True),
            StructField("gender_parity_in_account_ownership", DoubleType(), True),
            StructField("gender_parity_in_income", DoubleType(), True),
            StructField("wbl_score", DoubleType(), True),
            StructField("gender_gap_in_social_media_use", DoubleType(), True),
            StructField("gender_gap_in_mobile_ownership", DoubleType(), True),
            StructField("tlds_per_capita", DoubleType(), True),
            StructField("e_government_score", DoubleType(), True),
            StructField("mobile_social_media_penetration", DoubleType(), True),
            StructField("apps_developed_per_person", DoubleType(), True),
            StructField("number_of_apps_in_national_language", DoubleType(), True),
            StructField("accessibility_of_top_ranked_apps", DoubleType(), True),
            StructField("cybersecurity_index", DoubleType(), True),
        ]
    )


def _process_args():
    """Processes command line arguments"""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--schema", help="Name of the table to be written", required=True
    )

    parser.add_argument(
        "--csv-path", help="HDFS location for csv files to be read", required=True
    )

    parser.add_argument("--world-bank-year", help="World bank year", required=True)

    parser.add_argument("--score-year", help="Score year", required=True)

    parser.add_argument("--spi-year", help="SPI year", required=True)

    parser.add_argument("--year", help="Year to be used as a partition", required=True)

    parser.add_argument("--is-iceberg", help="Whether to write results to an iceberg table", default=False, required=False)

    args = parser.parse_args()

    return args


def _main(
    csv_path: str,
    schema: str,
    world_bank_year: int,
    score_year: int,
    spi_year: int,
    year: int,
    is_iceberg: bool = False,
) -> None:
    """
    Computes the affiliate input metrics and writes them to Hive

    param schema: Name of the schema to be written
    param hdfs_path: HDFS location for csv files to be read
    param partition_year: Year to be used as a partition
    """

    conf = SparkConf()
    conf.setAppName("access_input_metrics")
    spark = SparkSession.builder.config(conf=conf).getOrCreate()

    table_name = f"{schema}.access_input_metrics"

    df = spark.read.csv(csv_path, header=True)

    schema_mobile_connectivity_index = _get_mci_schema()

    df = Utils.convert_data_types(df, schema_mobile_connectivity_index)

    df.createOrReplaceTempView("mci_input_metrics")

    df = spark.sql(
        f"""
        WITH country_data AS (
    SELECT distinct country_code_iso_3,
            first_value(canonical_country_name) over(PARTITION BY country_code_iso_3)  canonical_country_name,
            country_code_iso_2
        FROM {schema}.country_meta_data country
        WHERE country_code_iso_3 IS NOT NULL
    ),

    world_bank AS (
    SELECT *
        FROM {schema}.world_bank_data_input_metrics
        WHERE time ='YR{world_bank_year}'
    ),
    mobile_connectivity_index AS (

    SELECT *
    FROM (
        SELECT iso_code,
                connectivity_index / lag(connectivity_index) over (partition by iso_code order by score_year)  as connectivity_index,
                connectivity_index as curr_year_value,
                lag(connectivity_index) over (partition by iso_code order by score_year) prev_year_value,
                score_year
        FROM mci_input_metrics
        WHERE score_year IN ({score_year}-1,{score_year})
        ) mci
    WHERE score_year = {score_year}
    )
    SELECT country.country_code_iso_3,
        COALESCE(population.internet_percent_annual_signal,0)                                  internet_percent_annual_signal,
        COALESCE(mobile.value,0)                                                               mobile_subscriptions_annual_signal,
        COALESCE(social.access_to_basic_knowledge,0)                                           access_to_basic_knowledge_annual_signal,
        COALESCE(social.access_to_information_and_communication,0)                             access_to_information_and_communication,
        COALESCE(mci.connectivity_index,0)                                                     annual_connectivity_index_annual_change,
        coalesce(population.internet_annual_change,0)                                          internet_annual_change
    FROM country_data country
    LEFT JOIN world_bank mobile ON (country.country_code_iso_3 = mobile.economy AND mobile.series = 'IT.CEL.SETS.P2')
    LEFT JOIN {schema}.social_progress_input_metrics social ON (country.country_code_iso_3 = social.country_code AND social.spi_year = {spi_year})
    LEFT JOIN {schema}.population_data_input_metrics population ON (country.country_code_iso_3 = population.country_code AND population.year = {year} - 1)
    LEFT JOIN mobile_connectivity_index mci ON (country.country_code_iso_3 = mci.iso_code)
        """
    )

    if is_iceberg:
        df.writeTo(table_name).overwritePartitions()
    else:
        Utils.write_results(year, spark, table_name, df)

    spark.stop()


def run():
    """Validates command line arguments and runs the main function"""

    args = _process_args()

    # Get the arguments and pass them to the _main function
    _main(
        schema=args.schema,
        csv_path=args.csv_path,
        score_year=args.score_year,
        spi_year=args.spi_year,
        world_bank_year=args.world_bank_year,
        year=args.year,
        is_iceberg=args.is_iceberg,
    )

if __name__ == "__main__":
    run()
