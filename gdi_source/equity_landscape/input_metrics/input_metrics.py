import argparse

from pyspark import SparkConf
from pyspark.sql import SparkSession


def main(year: int, schema: str, output_path: str) -> None:
    """
    Combines all the input metrics into one dataframe and saves it to HDFS.

    param int year: year
    param str schema: schema
    param str output_path: output path

    e.g.  /path/to/equity_landscape_input_metrics.py --year 2021 --schema gdi --output_path /path/to/output/

    """

    conf = SparkConf()
    conf.setAppName("equity_landscape_input_metrics")
    spark = SparkSession.builder.config(conf=conf).getOrCreate()

    spark.sql(
       f"""
        SELECT country.country_code_iso_2         country_code,
               count(affiliate.affiliate_country) count_affiliate_country
          FROM {schema}.affiliate_data_input_metrics affiliate,
               {schema}.country_meta_data country
         WHERE affiliate.affiliate_country = country.canonical_country_name
           AND affiliate.year = {year}
      GROUP BY country.country_code_iso_2;
       """
    ).createOrReplaceTempView("af_country")

    spark.sql(
       f"""
        SELECT country_code,
               count(distinct unique_grantee_id) count_unique_grantees
          FROM {schema}.grants_input_metrics
         WHERE calendar_year = {year}
         GROUP BY country_code
       """
    ).createOrReplaceTempView("grantees")


    df = spark.sql(
        f"""
        WITH country_data AS (
            SELECT distinct country_code_iso_3                                                                                             country_code_iso_3,
                    first_value(canonical_country_name) over(PARTITION BY country_code_iso_3)                                              country_name,
                    country_code_iso_2                                                                                                     country_code_iso_2,
                    continent_name                                                                                                         continent,
                    subcontinent_name                                                                                                      sub_continent
                FROM {schema}.country_meta_data country
                WHERE country_code_iso_3 IS NOT NULL
            )
        SELECT 'country'                                                                                                                    geography_level,
                country.country_code_iso_3                                                                                                  country_code_iso_3,
                country.country_code_iso_2                                                                                                  country_code_iso_2,
                country.country_name                                                                                                        country_name,
                country.continent                                                                                                           continent,
                country.sub_continent                                                                                                       sub_continent,
                affiliate.count_operating_affiliates                                                                                        count_operating_affiliates,
                affiliate.affiliate_size_max                                                                                                affiliate_size_max,
                round(affiliate.affiliate_tenure_max,2)                                                                                     affiliate_tenure_max,
                round(grants.sum_calendar_year_affiliate_grants/grants.sum_calendar_year_grants,2)                                          affiliate_percent_annual_grants,
                round(grants.sum_historical_affiliate_grants/grants.sum_historical_grants_to_date,2)                                        affiliate_percent_historical_grants,
                round(grants.sum_historical_affiliate_grants,2)                                                                             sum_historical_affiliate_grants,
                round(affiliate.affiliate_tenure_average,2)                                                                                 affiliate_tenure_average,
                affiliate.governance_type                                                                                                   affiliate_governance_type,
                affiliate.affiliate_size_growth                                                                                             affiliate_size_growth,
                round(grants.sum_historical_grants_to_date,2)                                                                               sum_historical_grants_to_date,
                round(grants.sum_calendar_year_grants,2)                                                                                    sum_calendar_year_grants,
                grants.count_historical_grants_to_date                                                                                      count_historical_grants_to_date,
                grants.count_calendar_year_grants                                                                                           count_calendar_year_grants,
                round(grants.annual_grants_weighted,2)                                                                                      annual_grants_weighted,
                round(grants.annual_affiliate_grants_weighted,2)                                                                            annual_affiliate_grants_weighted,
                round(grants.historical_grants_weighted,2)                                                                                  historical_grants_weighted,
                round(grants.historical_affiliate_grants_weighted,2)                                                                        historical_affiliate_grants_weighted,
                round(grants.annual_grants_change_rate,2)                                                                                   annual_grants_change_rate,
                round(grants.annual_grants_by_annual_change_rate,2)                                                                         annual_grants_by_annual_change_rate,
                round(grants.historical_grants_by_annual_change_rate,2)                                                                     historical_grants_by_annual_change_rate,
                round(grants.sum_calendar_year_affiliate_grants,2)                                                                          sum_calendar_year_affiliate_grants,
                grants.count_calendar_year_affiliate_grants                                                                                 count_calendar_year_affiliate_grants,
                grants.count_historical_affiliate_grants                                                                                    count_historical_affiliate_grants,
                round(pivot_yoy_change.commons,2)                                                                                           commons_annual_change,
                round(pivot_yoy_change.mediawiki,2)                                                                                         mediawiki_annual_change,
                round(pivot_yoy_change.wikidata,2)                                                                                          wikidata_annual_change,
                round(pivot_yoy_change.wikipedia,2)                                                                                         wikipedia_annual_change,
                round(pivot_yoy_change.wikisource,2)                                                                                        wikisource_annual_change,
                round(pivot_yoy_change.sister_project,2)                                                                                    sister_projects_annual_change,
                round(pivot_yoy_change.organizing_wiki,2)                                                                                   organizing_wiki_annual_change,
                round(pivot_monthly_bins.commons,2)                                                                                         commons_annual_signal,
                round(pivot_monthly_bins.mediawiki,2)                                                                                       mediawiki_annual_signal,
                round(pivot_monthly_bins.wikidata,2)                                                                                        wikidata_annual_signal,
                round(pivot_monthly_bins.wikipedia,2)                                                                                       wikipedia_annual_signal,
                round(pivot_monthly_bins.wikisource,2)                                                                                      wikisource_annual_signal,
                round(pivot_monthly_bins.sister_project,2)                                                                                  sister_projects_annual_signal,
                round(pivot_monthly_bins.organizing_wiki,2)                                                                                 organizing_wiki_annual_signal,
                round(online_metrics.percent_editors_active,2)                                                                              percent_editors_active_month,
                pageviews.metric_value                                                                                                      pageviews_annual_signal,
                yoy_pageviews.metric_value                                                                                                  pageviews_annual_change,
                unique_devices.metric_value                                                                                                 unique_devices_annual_signal,
                yoy_devices.metric_value                                                                                                    unique_devices_annual_change,
                round(access.internet_percent_annual_signal,2)                                                                              internet_percent_annual_signal,
                round(access.mobile_subscriptions_annual_signal,2)                                                                          mobile_subscriptions_annual_signal,
                round(access.access_to_basic_knowledge_annual_signal,2)                                                                     access_to_basic_knowledge_annual_signal,
                round(access.annual_connectivity_index_annual_change,2)                                                                     annual_connectivity_index_annual_change,
                round(access.internet_annual_change,2)                                                                                      internet_annual_change,
                round(access.access_to_information_annual_signal,2)                                                                         access_to_information_annual_signal,
                round(population.population_annual_signal,2)                                                                                population_annual_signal,
                population.population_annual_change                                                                                         population_annual_change,
                round(population.wikimedia_annual_signal,2)                                                                                 wikimedia_annual_signal,
                round(population.wikimedia_active_annual_signal,2)                                                                          wikimedia_active_annual_signal,
                round(population.gdp_per_capita_ppp_current,2)                                                                              gdp_per_capita_current,
                round(population.gdp_per_capita_ppp_constant,2)                                                                             gdp_per_capita_constant,
                programs_events.avg_monthly_organizers,
                programs_events.avg_monthly_participants,
                round(programs_events.non_affiliate_apg_grants_usd_total,2)                                                                 non_affiliate_apg_grants_usd_total,
                round(programs_events.non_apg_grants_usd_total,2)                                                                           non_apg_grants_usd_total,
                round(programs_events.non_affiliate_apg_grants_usd_percent,2)                                                               non_affiliate_apg_grants_usd_percent,
                programs_events.non_apg_grants_count,
                round(freedom.electoral_democracy_index,2)                                                                                  electoral_democracy_index,
                round(freedom.government_censorship_effort,2)                                                                               government_censorship_effort,
                round(freedom.press_freedom_index,2)                                                                                        press_freedom_index,
                round(freedom.control_of_corruption,2)                                                                                      control_of_corruption,
                round(freedom.yoy_electoral_democracy_index,2)                                                                              yoy_electoral_democracy_index,
                round(freedom.yoy_government_censorship_effort,2)                                                                           yoy_government_censorship_effort,
                round(freedom.yoy_press_freedom_index,2)                                                                                    yoy_press_freedom_index,
                round(freedom.yoy_control_of_corruption,2)                                                                                  yoy_control_of_corruption,
                af_country.count_affiliate_country,
                grantees.count_unique_grantees,
                {year}                                                                                                                      year
            FROM country_data country
            LEFT JOIN  {schema}.affiliate_leadership_input_metrics affiliate ON (affiliate.year = {year} AND country.country_code_iso_3 = affiliate.country_code)
            LEFT JOIN  {schema}.population_leadership_input_metrics population ON (population.year = {year} AND country.country_code_iso_3 = population.country_code)
            LEFT JOIN  {schema}.grants_leadership_input_metrics grants ON (grants.year = {year} AND country.country_code_iso_3 = grants.country_code)
            LEFT JOIN  {schema}.geoeditor_input_metrics_pivot pivot_yoy_change ON (pivot_yoy_change.year = {year} AND pivot_yoy_change.metric = 'yoy_change' AND country.country_code_iso_2 = pivot_yoy_change.country_code)
            LEFT JOIN  {schema}.geoeditor_input_metrics_pivot pivot_monthly_bins ON (pivot_monthly_bins.year = {year} AND pivot_monthly_bins.metric = 'monthly_bins' AND country.country_code_iso_2 = pivot_monthly_bins.country_code)
            LEFT JOIN  {schema}.geoeditor_online_input_metrics online_metrics ON (online_metrics.year = {year} AND country.country_code_iso_2 = online_metrics.country_code)
            LEFT JOIN  {schema}.georeadership_input_metrics yoy_pageviews ON (yoy_pageviews.year = {year} AND yoy_pageviews.metric = 'yoy_pageviews' AND country.country_code_iso_2 = yoy_pageviews.country_code)
            LEFT JOIN  {schema}.georeadership_input_metrics pageviews ON (pageviews.year = {year} AND pageviews.metric = 'pageviews' AND country.country_code_iso_2 = pageviews.country_code)
            LEFT JOIN  {schema}.georeadership_input_metrics unique_devices ON (unique_devices.year = {year} AND unique_devices.metric = 'unique_devices' AND country.country_code_iso_2 = unique_devices.country_code)
            LEFT JOIN  {schema}.georeadership_input_metrics yoy_devices ON (yoy_devices.year = {year} AND yoy_devices.metric = 'yoy_unique_devices' AND country.country_code_iso_2 = yoy_devices.country_code)
            LEFT JOIN  {schema}.access_input_metrics  access  ON (access.country_code = country.country_code_iso_3 AND access.year = {year})
            LEFT JOIN  {schema}.programs_events_input_metrics programs_events ON (programs_events.country_code = country.country_code_iso_2 AND programs_events.year = {year})
            LEFT JOIN  {schema}.freedom_leadership_input_metrics freedom ON (freedom.country_code = country.country_code_iso_3 AND freedom.year = {year})
            LEFT JOIN  af_country ON (af_country.country_code = country.country_code_iso_2)
            LEFT JOIN grantees ON (grantees.country_code = country.country_code_iso_3)
    """
    )

    df = df.fillna(0)

    df.write.mode("overwrite").parquet(output_path)

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("--schema", type=str, help="Hive schema to write to")
    parser.add_argument(
        "--year", type=str, help="Year for which metrics should be calculated for"
    )
    parser.add_argument("--output-path", type=str, help="Path to write output to")

    args = parser.parse_args()

    if args.year:
        year = int(args.year)
    else:
        raise ValueError("Year not specified")

    if args.schema:
        schema = args.schema
    else:
        raise ValueError("Schema not specified")

    if args.output_path:
        output_path = args.output_path
    else:
        raise ValueError("Output path not specified")

    output_path = f"{output_path}/year={year}"

    main(year=year, schema=schema, output_path=output_path)
