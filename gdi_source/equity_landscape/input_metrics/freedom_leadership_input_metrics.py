import argparse

from pyspark import SparkConf
from pyspark.sql import SparkSession

import gdi_source.util as Utils

"""
This module contains functions to generate affiliate input metrics for a given year.
"""


def _process_args()-> argparse.Namespace:
    """Processes command line arguments"""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--schema", help="Name of the table to be written", required=True
    )

    parser.add_argument("--year", help="Year to be used as a partition", required=True)

    parser.add_argument("--world-bank-year", help="World Bank year", required=True)

    parser.add_argument("--is-iceberg", help="Whether the table is iceberg or not", required=False, default=False)

    args = parser.parse_args()

    return args


def _main(
    schema: str,
    year: int,
    world_bank_year: int,
    is_iceberg: bool,
) -> None:
    """
    Computes the affiliate input metrics and writes them to Hive

    param schema: Name of the schema to be written
    param hdfs_path: HDFS location for csv files to be read
    param partition_year: Year to be used as a partition
    """

    conf = SparkConf()
    conf.setAppName("freedom_leadership_input_metrics")
    spark = SparkSession.builder.config(conf=conf).getOrCreate()

    table_name = f"{schema}.freedom_leadership_input_metrics"

    # Create the affiliates_by_geo view
    # This is done to get the affiliates by geo for the current year by exploding the operating countries
    # We trim the country code to remove the leading and trailing spaces

    df = spark.sql(
        f"""
            WITH v_dem AS (
                SELECT country_code,
                    v2x_polyarchy,
                    v2mecenefm_mean,
                    v2x_polyarchy_annual_change,
                    v2mecenefm_mean_annual_change
                FROM {schema}.vdem_input_metrics
                WHERE year = {year}
            ),
            cc_est AS (
            SELECT economy  as country_code,
                    value / lag(value) over (partition by economy order by time) as yoy_value,
                    value,
                    time
                FROM {schema}.world_bank_data_input_metrics
                WHERE time IN ('YR'||{world_bank_year}, 'YR'||({world_bank_year} - 1))
                AND series = 'CC.EST'
            ),
            freedom_index AS (
                SELECT country_code,
                        press_score,
                        press_score / lag(press_score) over (partition by country_code order by year) as yoy_value,
                        year
                FROM {schema}.freedom_input_metrics
                WHERE year IN ({year}, {year}-1)
            )
            SELECT country.country_code_iso_3,
                v2x_polyarchy                      AS electoral_democracy_index,
                v2mecenefm_mean                    AS govt_censorship_effort,
                freedom.press_score                AS press_freedom_index,
                cc_est.value                       AS control_of_corruption,
                v2x_polyarchy_annual_change        AS yoy_electoral_democracy_index,
                v2mecenefm_mean_annual_change      AS yoy_govt_censorship_effort,
                freedom.yoy_value                  AS yoy_press_freedom,
                cc_est.yoy_value                   AS yoy_corruption_control
            FROM {schema}.country_meta_data country
            LEFT JOIN cc_est ON (country.country_code_iso_3 = cc_est.country_code AND cc_est.time = 'YR' || {world_bank_year})
            LEFT JOIN v_dem ON (country.country_code_iso_3 = v_dem.country_code)
            LEFT JOIN freedom_index freedom ON (country.country_code_iso_3  = freedom.country_code AND freedom.year = {year})
        """
    )

    df = df.fillna(0)

    if is_iceberg:
        df.writeTo(table_name).overwritePartitions()
    else:
        Utils.write_results(year, spark, table_name, df)

    spark.stop()


def run():
    """Validates command line arguments and runs the main function"""

    args = _process_args()

    # Get the arguments and pass them to the _main function
    _main(
        schema=args.schema,
        year=args.year,
        world_bank_year=args.world_bank_year,
        is_iceberg=args.is_iceberg
    )


if __name__ == "__main__":
    run()
