import argparse

from pyspark import SparkConf
from pyspark.sql import SparkSession

import gdi_source.util as Utils

"""
This module contains functions to generate affiliate input metrics for a given year.
"""


def _process_args()-> argparse.Namespace:
    """Processes command line arguments"""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--schema", help="Name of the table to be written", required=True
    )

    parser.add_argument("--year", help="Year to be used as a partition", required=True)

    parser.add_argument("--is-iceberg", help="Whether the table is iceberg or not", required=False, default=False)

    args = parser.parse_args()

    return args


def _main(
    schema: str,
    year: int,
    is_iceberg: bool,
) -> None:
    """
    Computes the affiliate input metrics and writes them to Hive

    param schema: Name of the schema to be written
    param hdfs_path: HDFS location for csv files to be read
    param partition_year: Year to be used as a partition
    """

    conf = SparkConf()
    conf.setAppName("affiliate_input_metrics")
    spark = SparkSession.builder.config(conf=conf).getOrCreate()

    table_name = f"{schema}.affiliate_leadership_input_metrics"

    df = spark.sql(
        f"select * from {schema}.affiliate_data_input_metrics where year = {year}"
    )

    filter_clause = "length(country_code) = 3 OR country_code = 'INT\\'L'"

    df = df.where(filter_clause)

    df.createOrReplaceTempView("affiliate_data")

    columns_to_ignore = ["year", "operating_countries", "country_code"]

    columns = Utils.get_columns(df, columns_to_ignore)

    select_columns = Utils.create_select(columns)

    # Create the affiliates_by_geo view
    # This is done to get the affiliates by geo for the current year by exploding the operating countries
    # We trim the country code to remove the leading and trailing spaces

    spark.sql(
        f"""
        WITH affiliates AS (

            select {select_columns},explode(array_union(operating_countries, array(country_code))) as country_code  FROM affiliate_data
            )
            SELECT distinct {select_columns}, trim(country_code) as country_code FROM affiliates
        """
    ).where(filter_clause).createOrReplaceTempView("affiliates_by_geo")

    df = spark.sql(
        """

          WITH current_affiliates (

               SELECT country_code,
                      COUNT(1)                              AS count_operating_affiliates,
                      AVG(affiliate_tenure)                 AS affiliate_tenure_average
                 FROM affiliates_by_geo
               GROUP BY country_code
          ),
          aff_data AS (
               SELECT country_code,
                      MAX(affiliate_size)                   AS affiliate_size_max,
                      SUM(affiliate_size_annual_change)     AS affiliate_size_growth,
                      MAX(affiliate_tenure)                 AS affiliate_tenure_max,
                      MAX(governance_type)                  AS governance_type
                 FROM affiliate_data
                GROUP BY country_code
         ),
        affiliates AS (
        SELECT curr.country_code,
               curr.count_operating_affiliates,
               aff.affiliate_size_max,
               aff.affiliate_size_growth,
               aff.affiliate_tenure_max,
               curr.affiliate_tenure_average,
               aff.governance_type
        FROM current_affiliates curr
        LEFT JOIN aff_data aff ON (curr.country_code = aff.country_code))
        SELECT aff.country_code,
               coalesce(aff.count_operating_affiliates,0) as count_operating_affiliates,
               coalesce(aff.affiliate_size_max,0) as affiliate_size_max,
               coalesce(aff.affiliate_size_growth,0) as affiliate_size_growth,
               coalesce(aff.affiliate_tenure_max,0) as affiliate_tenure_max,
               coalesce(aff.governance_type,0) as governance_type,
               coalesce(aff.affiliate_tenure_average,0) as affiliate_tenure_average
           FROM affiliates aff
    """
    )

    if is_iceberg:
        df.writeTo(table_name).overwritePartitions()
    else:
        Utils.write_results(year, spark, table_name, df)

    spark.stop()


def run():
    """Validates command line arguments and runs the main function"""

    args = _process_args()

    # Get the arguments and pass them to the _main function
    _main(
        schema=args.schema,
        year=args.year,
        is_iceberg=args.is_iceberg
    )


if __name__ == "__main__":
    run()
