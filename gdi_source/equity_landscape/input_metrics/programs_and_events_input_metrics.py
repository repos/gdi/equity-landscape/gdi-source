import argparse

from pyspark import SparkConf
from pyspark.sql import SparkSession
from pyspark.sql import functions as F
from pyspark.sql.types import IntegerType, StringType, StructField, StructType

import gdi_source.util as Utils

"""
Program to run the programs input metrics job
"""


def _process_args():
    """Processes command line arguments"""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--schema", help="Name of the table to be written", required=True
    )

    parser.add_argument("--input-path", help="HDFS location for csv files to be read", required=True)

    parser.add_argument("--calendar-year", help="Calendar year for grants data", required=True)

    parser.add_argument("--year", help="Year to be used as a partition", required=True)

    args = parser.parse_args()

    return args


def _main(
    schema: str,
    calendar_year: int,
    input_path: str,
    year: int,
) -> None:
    """
    Computes the affiliate input metrics and writes them to Hive

    param schema: Name of the schema to be written
    param hdfs_path: HDFS location for csv files to be read
    param partition_year: Year to be used as a partition
    """

    conf = SparkConf()
    conf.setAppName("programs_events_input_metrics")
    spark = SparkSession.builder.config(conf=conf).getOrCreate()

    schema_programs_events = StructType([
        StructField("country_code", StringType(),False),
        StructField("organizer", IntegerType(),False),
        StructField("participant", IntegerType(),False)
    ])

    table_name = f"{schema}.programs_events_input_metrics"


    df = spark.read.csv(input_path, schema=schema_programs_events)

    df.createOrReplaceTempView("programs")

    str_affiliate_column = """
    CASE
        WHEN LOWER(org_type) IN ('non-affiliate', 'individual') THEN
            'Non-Affiliate'
        ELSE
            'Others'
    END
    """

    str_affiliate_program_column = """
    CASE
        WHEN UPPER(program) IN ('APG', 'SAPG') THEN
            'APG'
        ELSE
            'Others'
    END
    """

    spark.sql(
        f"""
        SELECT *
        FROM {schema}.grants_input_metrics
        WHERE calendar_year = {calendar_year}
        """
    ).withColumn("affiliate_category", F.expr(str_affiliate_column)) \
    .withColumn("program_category", F.expr(str_affiliate_program_column)) \
    .selectExpr("country_code", "usd_over_grant_life", "affiliate_category", "program_category") \
    .createOrReplaceTempView("grants_data")

    df_programs_input_metrics = spark.sql(
    f"""
    WITH  country_data AS (
                SELECT distinct country_code_iso_3                                                                                             country_code_iso_3,
                        first_value(canonical_country_name) over(PARTITION BY country_code_iso_3)                                              country_name,
                        country_code_iso_2                                                                                                     country_code_iso_2,
                        continent_name                                                                                                         continent,
                        subcontinent_name                                                                                                      sub_continent
                    FROM {schema}.country_meta_data country
                    WHERE country_code_iso_3 IS NOT NULL
                ),
    grants_input AS (
    SELECT  country_code,
            SUM(
                CASE
                    WHEN affiliate_category = 'Non-Affiliate' AND program_category = 'Others' THEN
                        usd_over_grant_life
                    ELSE
                        0
                    END
                ) AS non_affiliate_apg_grants_usd_total,
                SUM(
                CASE
                    WHEN program_category = 'Others' THEN
                        usd_over_grant_life
                    ELSE
                        0
                    END
                ) AS non_apg_grants_usd_total,
            COUNT(
                CASE
                    WHEN affiliate_category = 'Non-Affiliate' AND program_category = 'Others' THEN
                        1
                    END

                ) AS non_apg_grants_count
        FROM grants_data
        GROUP BY affiliate_category, program_category,country_code
    ),
    grants_output AS (

        SELECT country_code,
            sum(non_affiliate_apg_grants_usd_total) AS non_affiliate_apg_grants_usd_total,
            sum(non_apg_grants_usd_total)           AS non_apg_grants_usd_total,
            sum(non_apg_grants_count)               AS non_apg_grants_count
        FROM grants_input
        GROUP BY country_code

    )
    SELECT country.country_code_iso_2 AS country_code,
          programs.organizer,
          programs.participant,
          grants.non_affiliate_apg_grants_usd_total,
          grants.non_apg_grants_usd_total,
          CASE
            WHEN non_apg_grants_usd_total > 0 THEN
              round(((non_affiliate_apg_grants_usd_total / non_apg_grants_usd_total) * 100),2)
            ELSE
              0
          END AS non_affiliate_apg_grants_usd_percent,
          grants.non_apg_grants_count
        FROM country_data country
        LEFT JOIN grants_output grants ON (country.country_code_iso_3 = grants.country_code)
        LEFT JOIN programs ON (country.country_code_iso_2 = programs.country_code)
    """
    ).fillna(0)

    Utils.write_results(year, spark, table_name, df_programs_input_metrics)

    spark.stop()


def run():
    """Validates command line arguments and runs the main function"""

    args = _process_args()

    # Get the arguments and pass them to the _main function
    _main(
        schema=args.schema,
        calendar_year=args.calendar_year,
        input_path=args.input_path,
        year=args.year,
    )


if __name__ == "__main__":
    run()
