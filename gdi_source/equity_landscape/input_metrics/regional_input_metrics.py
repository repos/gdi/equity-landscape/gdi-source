import argparse

from pyspark import SparkConf
from pyspark.sql import SparkSession
from pyspark.sql.functions import lit, monotonically_increasing_id

from gdi_source.util import create_select, get_columns

"""
This module contains code for combining all the input metrics and saving it to regional input metrics.

Functions:
    main(year: int, output_path: str, input_path: str) -> None:
        The main function to execute the Spark job that combines all the input metrics and save it to regional input metrics.
    _process_args() -> argparse.Namespace:
        Parses command line arguments and returns an argparse.Namespace object.



"""


def main(year: int, output_path: str, input_path: str, schema: str) -> None:
    """
    Combines all the input metrics and save it to regional input metrics
    param int year: year
    param str output_path: output path
    param str input_path: input path

    e.g.  /path/to/equity_landscape_input_metrics.py --year 2021 --output_path /path/to/output/ --input_path /path/to/input/

    """

    conf = SparkConf()
    conf.setAppName("equity_landscape_regional_input_metrics")
    spark = SparkSession.builder.config(conf=conf).getOrCreate()

    # Read the input metrics
    df_input_metrics = spark.read.parquet(input_path).drop("year")

    df_input_metrics.createOrReplaceTempView("input_metrics")

    lst_columns = get_columns(df_input_metrics, ["count_unique_grantees"])

    final_columns =  create_select(lst_columns, "input_metrics")

    df = spark.sql(
        f"""
            WITH regional_agg AS (
                    SELECT
                        geography_level AS geography_level_temp,
                        sub_continent,
                        continent,
                        global,
                        SUM(COALESCE(count_operating_affiliates, 0))                                AS count_operating_affiliates,
                        MAX(COALESCE(affiliate_size_max, 0))                                        AS affiliate_size_max,
                        MAX(COALESCE(affiliate_tenure_max, 0))                                      AS affiliate_tenure_max,
                        AVG(COALESCE(affiliate_tenure_average, 0))                                  AS affiliate_tenure_average,
                        SUM(COALESCE(affiliate_percent_annual_grants, 0))                           AS affiliate_percent_annual_grants,
                        SUM(COALESCE(affiliate_percent_historical_grants, 0))                       AS affiliate_percent_historical_grants,
                        AVG(COALESCE(affiliate_governance_type, 0))                                 AS affiliate_governance_type,
                        SUM(COALESCE(count_historical_affiliate_grants, 0))                         AS count_historical_affiliate_grants,
                        SUM(COALESCE(count_calendar_year_affiliate_grants, 0))                      AS count_calendar_year_affiliate_grants,
                        SUM(COALESCE(sum_calendar_year_affiliate_grants, 0))                        AS sum_calendar_year_affiliate_grants,
                        SUM(COALESCE(sum_historical_affiliate_grants, 0))                           AS sum_historical_affiliate_grants,
                        SUM(COALESCE(annual_affiliate_grants_weighted, 0))                          AS annual_affiliate_grants_weighted,
                        SUM(COALESCE(sum_historical_grants_to_date, 0))                             AS sum_historical_grants_to_date,
                        SUM(COALESCE(historical_affiliate_grants_weighted, 0))                      AS historical_affiliate_grants_weighted,
                        SUM(COALESCE(affiliate_size_growth, 0))                                     AS affiliate_size_growth,
                        SUM(COALESCE(sum_calendar_year_grants, 0))                                  AS sum_calendar_year_grants,
                        SUM(COALESCE(count_historical_grants_to_date, 0))                           AS count_historical_grants_to_date,
                        SUM(COALESCE(count_calendar_year_grants, 0))                                AS count_calendar_year_grants,
                        SUM(COALESCE(annual_grants_weighted, 0))                                    AS annual_grants_weighted,
                        SUM(COALESCE(historical_grants_weighted, 0))                                AS historical_grants_weighted,
                        AVG(COALESCE(annual_grants_change_rate, 0))                                 AS annual_grants_change_rate,
                        AVG(COALESCE(annual_grants_by_annual_change_rate, 0))                       AS annual_grants_by_annual_change_rate,
                        AVG(COALESCE(historical_grants_by_annual_change_rate, 0))                   AS historical_grants_by_annual_change_rate,
                        AVG(COALESCE(commons_annual_change, 0))                                     AS commons_annual_change,
                        AVG(COALESCE(mediawiki_annual_change, 0))                                   AS mediawiki_annual_change,
                        AVG(COALESCE(wikidata_annual_change, 0))                                    AS wikidata_annual_change,
                        AVG(COALESCE(wikipedia_annual_change, 0))                                   AS wikipedia_annual_change,
                        AVG(COALESCE(wikisource_annual_change, 0))                                  AS wikisource_annual_change,
                        AVG(COALESCE(sister_projects_annual_change, 0))                             AS sister_projects_annual_change,
                        AVG(COALESCE(organizing_wiki_annual_change, 0))                             AS organizing_wiki_annual_change,
                        SUM(COALESCE(commons_annual_signal, 0))                                     AS commons_annual_signal,
                        SUM(COALESCE(mediawiki_annual_signal, 0))                                   AS mediawiki_annual_signal,
                        SUM(COALESCE(wikidata_annual_signal, 0))                                    AS wikidata_annual_signal,
                        SUM(COALESCE(wikipedia_annual_signal, 0))                                   AS wikipedia_annual_signal,
                        SUM(COALESCE(wikisource_annual_signal, 0))                                  AS wikisource_annual_signal,
                        SUM(COALESCE(sister_projects_annual_signal, 0))                             AS sister_projects_annual_signal,
                        SUM(COALESCE(organizing_wiki_annual_signal, 0))                             AS organizing_wiki_annual_signal,
                        SUM(COALESCE(percent_editors_active_month, 0))                              AS percent_editors_active_month,
                        SUM(COALESCE(pageviews_annual_signal, 0))                                   AS pageviews_annual_signal,
                        AVG(COALESCE(pageviews_annual_change, 0))                                   AS pageviews_annual_change,
                        SUM(COALESCE(unique_devices_annual_signal, 0))                              AS unique_devices_annual_signal,
                        AVG(COALESCE(unique_devices_annual_change, 0))                              AS unique_devices_annual_change,
                        AVG(COALESCE(mobile_subscriptions_annual_signal, 0))                        AS mobile_subscriptions_annual_signal,
                        AVG(COALESCE(access_to_basic_knowledge_annual_signal, 0))                   AS access_to_basic_knowledge_annual_signal,
                        AVG(COALESCE(annual_connectivity_index_annual_change, 0))                   AS annual_connectivity_index_annual_change,
                        AVG(COALESCE(internet_annual_change, 0))                                    AS internet_annual_change,
                        AVG(COALESCE(access_to_information_annual_signal, 0))                       AS access_to_information_annual_signal,
                        SUM(COALESCE(population_annual_signal, 0))                                  AS population_annual_signal,
                        AVG(COALESCE(population_annual_change, 0))                                  AS population_annual_change,
                        SUM(COALESCE(wikimedia_annual_signal, 0))                                   AS wikimedia_annual_signal,
                        SUM(COALESCE(wikimedia_active_annual_signal, 0))                            AS wikimedia_active_annual_signal,
                        SUM(COALESCE(gdp_per_capita_current, 0))                                    AS gdp_per_capita_current,
                        SUM(COALESCE(gdp_per_capita_constant, 0))                                   AS gdp_per_capita_constant,
                        AVG(COALESCE(internet_percent_annual_signal, 0))                            AS internet_percent_annual_signal,
                        SUM(COALESCE(avg_monthly_organizers, 0))                                    AS avg_monthly_organizers,
                        SUM(COALESCE(avg_monthly_participants, 0))                                  AS avg_monthly_participants,
                        AVG(COALESCE(non_affiliate_apg_grants_usd_total, 0))                        AS non_affiliate_apg_grants_usd_total,
                        AVG(COALESCE(non_apg_grants_usd_total, 0))                                  AS non_apg_grants_usd_total,
                        AVG(COALESCE(non_affiliate_apg_grants_usd_percent, 0))                      AS non_affiliate_apg_grants_usd_percent,
                        SUM(COALESCE(non_apg_grants_count, 0))                                      AS non_apg_grants_count,
                        SUM(COALESCE(electoral_democracy_index, 0))                                 AS electoral_democracy_index,
                        SUM(COALESCE(government_censorship_effort, 0))                              AS government_censorship_effort,
                        SUM(COALESCE(press_freedom_index, 0))                                       AS press_freedom_index,
                        SUM(COALESCE(control_of_corruption, 0))                                     AS control_of_corruption,
                        SUM(COALESCE(yoy_electoral_democracy_index, 0))                             AS yoy_electoral_democracy_index,
                        SUM(COALESCE(yoy_government_censorship_effort, 0))                          AS yoy_government_censorship_effort,
                        SUM(COALESCE(yoy_press_freedom_index, 0))                                   AS yoy_press_freedom_index,
                        SUM(COALESCE(yoy_control_of_corruption, 0))                                 AS yoy_control_of_corruption,
                        SUM(COALESCE(count_affiliate_country, 0))                                   AS count_affiliate_country,
                        COALESCE(COUNT(DISTINCT unique_grantee_id),0)                               AS count_unique_grantees
                        FROM (
                            SELECT
                                {final_columns},
                                'global' AS global,
                                grants_input_metrics.unique_grantee_id
                            FROM
                                input_metrics
                            LEFT JOIN {schema}.grants_input_metrics ON (input_metrics.country_code_iso_3 = grants_input_metrics.country_code
                                                                        AND grants_input_metrics.calendar_year = {year}
                                                                        )
                        )
                    WHERE
                        geography_level = 'country'
                    GROUP BY GROUPING SETS (
                        (geography_level, sub_continent),
                        (geography_level, continent),
                        (geography_level, global))),
                    regional_agg_stack AS (
                        SELECT
                            STACK(3, 'sub_continent', sub_continent, 'continent', continent, 'global', global) AS (geography_level, country_name),
                            agg.*
                        FROM
                            regional_agg agg
                    )
            SELECT *
            FROM regional_agg_stack
            WHERE country_name IS NOT NULL
        """
    )

    df = df.fillna(0)

    df = df.withColumn("country_code_iso_3", df["country_name"])

    df = df.withColumn("country_code_iso_2", df["country_name"])

    columns = [
        "geography_level",
        "country_code_iso_3",
        "country_code_iso_2",
        "country_name",
        "continent",
        "sub_continent",
    ]

    df_metrics_metadata = df.select(*columns).withColumn(
        "id", monotonically_increasing_id()
    )

    df_metrics_values = df.drop(*columns).withColumn(
        "id", monotonically_increasing_id()
    )

    df_regional_input_metrics = df_metrics_metadata.join(
        df_metrics_values, on="id", how="inner"
    ).drop("id")

    df_regional_input_metrics = df_regional_input_metrics.drop(
        "geography_level_temp", "global"
    )

    df_regional_input_metrics.createOrReplaceTempView("regional_input_metrics")

    df_countries = spark.sql(
        """
    SELECT *
    FROM input_metrics
    WHERE geography_level = 'country'
    ORDER BY country_name
    """
    )

    # Create the sub_continents DataFrame
    df_sub_continents = spark.sql(
        """
        SELECT *
        FROM regional_input_metrics
        WHERE geography_level = 'sub_continent'
        ORDER BY country_name
    """
    )

    # Create the continents DataFrame
    df_continents = spark.sql(
        """
        SELECT *
        FROM regional_input_metrics
        WHERE geography_level = 'continent'
        ORDER BY country_name
    """
    )

    # Create the global DataFrame
    df_global = spark.sql(
        """
        SELECT *
        FROM regional_input_metrics
        WHERE geography_level = 'global'
        ORDER BY country_name
    """
    )

    df_regional_input_metrics = (
        df_countries.unionByName(df_sub_continents)
        .unionByName(df_continents)
        .unionByName(df_global)
    )

    df_regional_input_metrics = df_regional_input_metrics.withColumn("year", lit(year))

    df_regional_input_metrics.write.mode("overwrite").parquet(output_path)


def _process_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--year", type=str, help="Year for which metrics should be calculated for", required=True
    )
    parser.add_argument("--output-path", type=str, help="Path to write output to", required=True)

    parser.add_argument("--input-path", type=str, help="Path to read input from", required=True)

    parser.add_argument("--schema", type=str, help="Schema to read grants data froms", default="gdi")

    return parser.parse_args()

if __name__ == "__main__":

    args = _process_args()

    input_path = f"{args.input_path}/year={args.year}"
    output_path = f"{args.output_path}/year={args.year}"
    main(year=args.year, output_path=output_path, input_path=input_path, schema=args.schema)
