import argparse

from pyspark import SparkConf
from pyspark.sql import SparkSession


def main(year: int, output_path: str, input_path: str) -> None:
    """
    Takes in the input ranks and writes the output ranks to the output path
    param int year: year
    param str output_path: output path
    param str input_path: input path

    e.g.  /path/to/output_metrics.py --year 2021 --output_path /path/to/output/ --input_path /path/to/input/

    """

    conf = SparkConf()
    conf.setAppName("equity_landscape_output_metrics")
    spark = SparkSession.builder.config(conf=conf).getOrCreate()

    spark.catalog.clearCache()  # Clear the cache

    # Read the input metrics
    spark.read.parquet(input_path).drop("year").createOrReplaceTempView("input_metrics")

    spark.catalog.cacheTable("input_metrics")  # Cache the table

    _readership_metrics(spark)

    spark.catalog.cacheTable("readership_output_ranks")  # Cache the table

    _editorship_metrics(spark)

    spark.catalog.cacheTable("editorship_output_ranks")  # Cache the table

    _affiliate_metrics(spark)

    spark.catalog.cacheTable("affiliates_output_ranks")  # Cache the table

    _grants_metrics(spark)

    spark.catalog.cacheTable("grants_output_ranks")  # Cache the table

    _programs_and_events_metrics(spark)

    spark.catalog.cacheTable("programs_and_events_output_ranks")  # Cache the table

    _overall_engagement_metrics(spark)

    spark.catalog.cacheTable("overall_engagement_output_rank")  # Cache the table

    _population_metrics(spark)

    spark.catalog.cacheTable("population_output_ranks")  # Cache the table

    _access_metrics(spark)

    spark.catalog.cacheTable("access_output_ranks")  # Cache the table

    _freedom_metrics(spark)

    spark.catalog.cacheTable("freedom_output_ranks")  # Cache the table

    df = spark.sql(
        f"""
        WITH
            output_ranks AS (
                SELECT * FROM readership_output_ranks
                    UNION ALL
                SELECT * FROM editorship_output_ranks
                    UNION ALL
                SELECT * FROM affiliates_output_ranks
                    UNION ALL
                SELECT * FROM grants_output_ranks
                    UNION ALL
                SELECT * FROM overall_engagement_output_rank
                    UNION ALL
                SELECT * FROM population_output_ranks
                    UNION ALL
                SELECT * FROM access_output_ranks
                    UNION ALL
                SELECT * FROM programs_and_events_output_ranks
                    UNION ALL
                SELECT * FROM freedom_output_ranks
             ),
            output_ranks_excl_global AS (
                SELECT
                    op.*,
                    ip.country_name,
                    ip.sub_continent,
                    ip.continent
                FROM
                    output_ranks op
                LEFT JOIN
                    input_metrics ip
                    ON op.geography_level = ip.geography_level AND op.country_code = ip.country_code_iso_2
                WHERE
                    op.geography_level <> 'global'),

            global_ranks AS (
                SELECT
                    'global' AS geography_level,
                    'global' AS country_code,
                    AVG(metric_value) AS metric_value,
                    metric,
                    'global' AS country_name,
                    NULL AS sub_continent,
                    NULL AS continent
                FROM
                    output_ranks_excl_global
                WHERE
                    geography_level = 'continent'
                GROUP BY
                    metric),

            output_ranks_pivot AS (
                SELECT
                    *
                FROM (
                    SELECT * FROM output_ranks_excl_global
                        UNION ALL
                    SELECT * FROM global_ranks)
                PIVOT (
                    SUM(metric_value)
                    FOR
                        metric IN ('reader_presence', 'reader_growth', 'readership',
                                    'editor_presence', 'editor_growth', 'editorship',
                                    'affiliate_presence', 'affiliate_growth',
                                    'affiliates_leadership', 'grants_presence', 'grants_growth',
                                    'grants_leadership', 'overall_engagement', 'population',
                                    'editor_population_penetration', 'percent_global_editors',
                                    'percent_global__active_editors', 'population_underrepresentation',
                                    'access_presence', 'access_growth', 'access', 'programs_leadership',
                                    'overall_enablement', 'freedom_growth', 'freedom', 'freedom_presence'))
                ORDER BY
                    CASE
                        WHEN geography_level = 'country' THEN 1
                        WHEN geography_level = 'sub_continent' THEN 2
                        WHEN geography_level = 'continent' THEN 3
                        WHEN geography_level = 'global' THEN 4
                    END,
                    country_code)

        SELECT *,
               {year} AS year
        FROM output_ranks_pivot
    """
    )

    df = df.replace(float("nan"), None)

    df.write.mode("overwrite").parquet(output_path)


def _population_metrics(spark):
    spark.sql(
        """
        WITH
        population_input_metrics_hstack AS (
            SELECT
                ip.geography_level,
                country_code_iso_2 AS country_code,
                COALESCE(population_annual_signal, 0)/1000  AS population_annual_signal,
                COALESCE(population_annual_change, 0) AS population_annual_change,
                COALESCE(wikipedia_annual_signal, 0) AS wikipedia_annual_signal,
                COALESCE(wikimedia_annual_signal, 0) AS wikimedia_annual_signal,
                COALESCE(wikimedia_active_annual_signal, 0) AS wikimedia_active_annual_signal,
                COALESCE(oe.metric_value, 0) AS overall_engagement
            FROM
                input_metrics ip
            LEFT JOIN
                overall_engagement_output_rank oe
            ON
                ip.geography_level = oe.geography_level AND ip.country_code_iso_2 = oe.country_code),
        presence AS (
            SELECT
                geography_level,
                country_code,
                ROUND(
                    PERCENT_RANK() OVER (
                        PARTITION BY geography_level
                        ORDER BY population_annual_signal * (1+ population_annual_change))*10, 2) AS metric_value,
                'population' AS metric
            FROM
                population_input_metrics_hstack),
        editor_population_penetration AS (
            SELECT
                geography_level,
                country_code,
                ROUND(
                    PERCENT_RANK() OVER (
                        PARTITION BY geography_level
                        ORDER BY COALESCE(wikipedia_annual_signal / population_annual_signal, 0))*10, 2) AS metric_value,
                'editor_population_penetration' AS metric
            FROM
                population_input_metrics_hstack),
        editor_totals AS (
            SELECT
                geography_level,
                SUM(wikimedia_annual_signal) AS total_global_editors,
                SUM(wikimedia_active_annual_signal) AS total_global_active_editors
            FROM
                population_input_metrics_hstack
            GROUP BY
                geography_level),
        percent_global_editors AS (
            SELECT
                ip.geography_level,
                country_code,
                ROUND(wikimedia_annual_signal / total_global_editors * 100, 2) AS metric_value,
                'percent_global_editors' AS metric
            FROM
                population_input_metrics_hstack ip
            LEFT JOIN
                editor_totals et
            ON
                ip.geography_level = et.geography_level),
        percent_global_active_editors AS (
            SELECT
                ip.geography_level,
                country_code,
                ROUND(wikimedia_active_annual_signal / total_global_active_editors * 100, 2) AS metric_value,
                'percent_global__active_editors' AS metric
            FROM
                population_input_metrics_hstack ip
            LEFT JOIN
                editor_totals et
            ON
                ip.geography_level = et.geography_level),
        underrepresentation_input AS (
            SELECT
                ip.geography_level,
                ip.country_code,
                overall_engagement,
                p.metric_value AS population
            FROM
                population_input_metrics_hstack ip
            LEFT JOIN
                presence p
            ON
                ip.geography_level = p.geography_level AND ip.country_code = p.country_code),
        underrepresentation AS (
            SELECT
                geography_level,
                country_code,
                ROUND(
                    10 - PERCENT_RANK() OVER (
                                            PARTITION BY geography_level
                                            ORDER BY overall_engagement - population)*10, 2) AS metric_value,
                'population_underrepresentation' AS metric
            FROM
                underrepresentation_input)
    SELECT * FROM presence
    UNION ALL
    SELECT * FROM editor_population_penetration
    UNION ALL
    SELECT * FROM percent_global_editors
    UNION ALL
    SELECT * FROM percent_global_active_editors
    UNION ALL
    SELECT * FROM underrepresentation
    """
    ).createOrReplaceTempView("population_output_ranks")

def _freedom_metrics(spark):

    spark.sql(
        """
            WITH freedom_input_metrics_hstack AS (
                    SELECT
                        geography_level,
                        country_code_iso_2 AS country_code,
                        COALESCE(electoral_democracy_index, 0) AS electoral_democracy_index,
                        COALESCE(government_censorship_effort, 0) AS government_censorship_effort,
                        COALESCE(1 - press_freedom_index, 0) AS press_freedom_index,
                        COALESCE(control_of_corruption, 0) AS control_of_corruption,
                        COALESCE(yoy_electoral_democracy_index, 0) AS yoy_electoral_democracy_index,
                        COALESCE(yoy_government_censorship_effort, 0) AS yoy_government_censorship_effort,
                        COALESCE(1 - yoy_press_freedom_index, 0) AS yoy_press_freedom_index,
                        COALESCE(yoy_control_of_corruption, 0) AS yoy_control_of_corruption
                FROM input_metrics
                ),
                freedom_input_metrics_vstack AS (
                    SELECT
                        geography_level,
                        country_code,
                        metric,
                        STACK(8,
                            electoral_democracy_index, 'electoral_democracy_index',
                            government_censorship_effort, 'government_censorship_effort',
                            press_freedom_index, 'press_freedom_index',
                            control_of_corruption, 'control_of_corruption',
                            yoy_electoral_democracy_index, 'yoy_electoral_democracy_index',
                            yoy_government_censorship_effort, 'yoy_government_censorship_effort',
                            yoy_press_freedom_index, 'yoy_press_freedom_index',
                            yoy_control_of_corruption, 'yoy_control_of_corruption'
                        ) AS (value, metric)
                    FROM freedom_input_metrics_hstack
                ),
                freedom_input_metrics_ranks AS (
                    SELECT
                        geography_level,
                        country_code,
                        metric,
                        PERCENT_RANK() OVER (PARTITION BY geography_level, metric ORDER BY value) as p_rank
                    FROM freedom_input_metrics_vstack
                ),
                presence_avg_input AS (
                SELECT geography_level,
                        country_code,
                        ROUND(PERCENT_RANK() OVER (PARTITION BY geography_level ORDER BY AVG(p_rank))*10, 2) AS metric_value,
                        'freedom_presence' AS metric
                    FROM freedom_input_metrics_ranks
                WHERE metric IN ('electoral_democracy_index',
                                 'press_freedom_index',
                                 'control_of_corruption',
                                 'government_censorship_effort'
                                )
                    GROUP BY geography_level, country_code
                ),
                growth_avg_input AS (
                SELECT geography_level,
                        country_code,
                        ROUND(PERCENT_RANK() OVER (PARTITION BY geography_level ORDER BY AVG(p_rank))*10, 2) AS metric_value,
                        'freedom_growth' AS metric
                    FROM freedom_input_metrics_ranks
                WHERE metric IN ('yoy_electoral_democracy_index',
                                 'yoy_government_censorship_effort',
                                 'yoy_press_freedom_index',
                                 'yoy_control_of_corruption'
                                )
                    GROUP BY geography_level, country_code
                ),
                presence_growth_ranks AS (
                    SELECT *
                    FROM (
                        SELECT geography_level, country_code, metric, p_rank
                          FROM freedom_input_metrics_ranks
                    )
                PIVOT (
                    MAX(p_rank)
                    FOR metric IN ('control_of_corruption', 'electoral_democracy_index', 'government_censorship_effort', 'press_freedom_index')
                    )),
                    presence_growth_input AS (
                    SELECT hstack.geography_level,
                            hstack.country_code,
                            AVG((hstack.electoral_democracy_index * hstack.government_censorship_effort)
                            + (hstack.yoy_government_censorship_effort * ranks.government_censorship_effort)
                            + (hstack.yoy_press_freedom_index * ranks.press_freedom_index)
                            + (hstack.yoy_control_of_corruption * ranks.control_of_corruption)
                            ) AS presence_growth
                    FROM freedom_input_metrics_hstack hstack,
                        presence_growth_ranks ranks
                    WHERE hstack.geography_level = ranks.geography_level
                    AND hstack.country_code = ranks.country_code
                    GROUP BY hstack.geography_level, hstack.country_code
                    ),

                presence_growth_output AS (
                    SELECT
                        geography_level,
                        country_code,
                        ROUND(PERCENT_RANK() OVER (PARTITION BY geography_level ORDER BY presence_growth) * 10, 2) metric_value,
                        'freedom'                     AS metric
                    FROM presence_growth_input
                ),
                access (
                SELECT geography_level,
                        country_code,
                        round(metric_value,0) value
                FROM access_output_ranks
                WHERE metric = 'access'
                ),
                overall_enablement as (
                SELECT presence.geography_level,
                        presence.country_code,
                        AVG(round(access.value, 0) + presence.metric_value) metric_value,
                        'overall_enablement'    as metric
                    FROM access,
                        presence_growth_output presence
                WHERE access.geography_level = presence.geography_level
                    AND access.country_code = presence.country_code
                GROUP BY presence.geography_level,
                            presence.country_code
                )
                SELECT * FROM presence_avg_input
                UNION ALL
                SELECT * FROM presence_growth_output
                UNION ALL
                SELECT * FROM growth_avg_input
                UNION ALL
                SELECT * FROM overall_enablement
        """
    ).createOrReplaceTempView("freedom_output_ranks")


def _readership_metrics(spark: SparkSession) -> None:
    spark.sql(
        """
        WITH
            readership_input_metrics_hstack AS (
                SELECT
                    geography_level,
                    country_code_iso_2 AS country_code,
                    COALESCE(pageviews_annual_signal, 0) AS pageviews_annual_signal,
                    COALESCE(pageviews_annual_change, 0) AS pageviews_annual_change,
                    COALESCE(unique_devices_annual_signal, 0) AS unique_devices_annual_signal,
                    COALESCE(unique_devices_annual_change, 0) AS unique_devices_annual_change
                FROM
                    input_metrics
            ),

            readership_input_metrics_vstack AS (
                SELECT
                    geography_level,
                    country_code,
                    STACK(4, pageviews_annual_signal, 'pageviews_annual_signal',
                            pageviews_annual_change, 'pageviews_annual_change',
                            unique_devices_annual_signal, 'unique_devices_annual_signal',
                            unique_devices_annual_change, 'unique_devices_annual_change')
                        AS (metric_value, metric)
                FROM
                    readership_input_metrics_hstack),

            presence_growth_ranks AS (
                SELECT
                    geography_level,
                    country_code,
                    metric,
                    ROUND(
                        PERCENT_RANK() OVER (
                            PARTITION BY geography_level, metric
                            ORDER BY metric_value), 3) AS p_rank
                FROM
                    readership_input_metrics_vstack),

            presence AS (
                SELECT
                    geography_level,
                    country_code,
                    ROUND(
                        PERCENT_RANK() OVER (
                            PARTITION BY geography_level
                            ORDER BY AVG(p_rank))*10, 2) AS metric_value,
                    'reader_presence' AS metric
                    FROM
                        presence_growth_ranks
                    WHERE
                        metric IN ('pageviews_annual_signal', 'unique_devices_annual_signal')
                    GROUP BY
                        geography_level, country_code),

            growth AS (
                SELECT
                    geography_level,
                    country_code,
                    ROUND(
                        PERCENT_RANK() OVER (
                            PARTITION BY geography_level
                            ORDER BY AVG(p_rank))*10, 2) AS metric_value,
                    'reader_growth' AS metric
                    FROM
                        presence_growth_ranks
                    WHERE
                        metric IN ('pageviews_annual_change', 'unique_devices_annual_change')
                    GROUP BY
                        geography_level, country_code),

            readership_inputs AS (
                SELECT
                    geography_level,
                    country_code,
                    STACK(2, pageviews_annual_signal*pageviews_annual_change, 'pageviews_r',
                            unique_devices_annual_signal*unique_devices_annual_change, 'devices_r') AS (value, input_label)
                FROM
                    readership_input_metrics_hstack),

            readership AS (
                SELECT
                    geography_level,
                    country_code,
                    ROUND(
                        PERCENT_RANK() OVER (
                            PARTITION BY geography_level
                            ORDER BY AVG(value))*10, 2) AS metric_value,
                    'readership' AS metric
                    FROM
                        readership_inputs
                    GROUP BY
                        geography_level, country_code)

        SELECT * FROM presence
        UNION ALL
        SELECT * FROM growth
        UNION ALL
        SELECT * FROM readership
        """
    ).createOrReplaceTempView("readership_output_ranks")


def _editorship_metrics(spark):
    spark.sql(
        """
        WITH
            editorship_input_metrics_hstack AS (
                SELECT
                    geography_level,
                    country_code_iso_2 AS country_code,
                    COALESCE(commons_annual_signal, 0) AS commons_annual_signal,
                    COALESCE(mediawiki_annual_signal, 0) AS mediawiki_annual_signal,
                    COALESCE(wikidata_annual_signal, 0) AS wikidata_annual_signal,
                    COALESCE(wikipedia_annual_signal, 0) AS wikipedia_annual_signal,
                    COALESCE(wikisource_annual_signal, 0) AS wikisource_annual_signal,
                    COALESCE(commons_annual_change, 0) AS commons_annual_change,
                    COALESCE(mediawiki_annual_change, 0) AS mediawiki_annual_change,
                    COALESCE(wikidata_annual_change, 0) AS wikidata_annual_change,
                    COALESCE(wikipedia_annual_change, 0) AS wikipedia_annual_change,
                    COALESCE(wikisource_annual_change, 0) AS wikisource_annual_change,
                    COALESCE(percent_editors_active_month, 0) AS percent_active_editors
                FROM
                    input_metrics),

            editorship_input_metrics_vstack AS (
                SELECT
                    geography_level,
                    country_code,
                    STACK(11,
                        commons_annual_signal, 'commons_annual_signal',
                        mediawiki_annual_signal, 'mediawiki_annual_signal',
                        wikidata_annual_signal, 'wikidata_annual_signal',
                        wikipedia_annual_signal, 'wikipedia_annual_signal',
                        wikisource_annual_signal, 'wikisource_annual_signal',
                        commons_annual_change, 'commons_annual_change',
                        mediawiki_annual_change, 'mediawiki_annual_change',
                        wikidata_annual_change, 'wikidata_annual_change',
                        wikipedia_annual_change, 'wikipedia_annual_change',
                        wikisource_annual_change, 'wikisource_annual_change',
                        percent_active_editors, 'percent_active_editors') AS (metric_value, metric)
                FROM
                    editorship_input_metrics_hstack),

            presence_growth_metric_ranks AS (
                SELECT
                    geography_level,
                    country_code,
                    metric,
                    ROUND(
                        PERCENT_RANK() OVER (
                            PARTITION BY geography_level, metric
                            ORDER BY metric_value), 3) AS p_rank
                FROM
                    editorship_input_metrics_vstack),

            presence AS (
                SELECT
                    geography_level,
                    country_code,
                    ROUND(
                        PERCENT_RANK() OVER(
                            PARTITION BY geography_level
                            ORDER BY AVG(p_rank))*10, 2) AS metric_value,
                    'editor_presence' AS metric
                FROM
                    presence_growth_metric_ranks
                WHERE
                    metric IN ('commons_annual_signal', 'mediawiki_annual_signal',
                            'wikidata_annual_signal', 'wikipedia_annual_signal', 'wikisource_annual_signal')
                GROUP BY
                    geography_level, country_code),

            growth AS (
                SELECT
                    geography_level,
                    country_code,
                    ROUND(
                        PERCENT_RANK() OVER(
                            PARTITION BY geography_level
                            ORDER BY AVG(p_rank))*10, 2) AS metric_value,
                    'editor_growth' AS metric
                FROM
                    presence_growth_metric_ranks
                WHERE
                    metric IN ('commons_annual_change', 'mediawiki_annual_change',
                            'wikidata_annual_change', 'wikipedia_annual_change', 'wikisource_annual_change')
                GROUP BY
                    geography_level, country_code),

            project_editorship_values AS (
                SELECT
                    geography_level,
                    country_code,
                    CASE WHEN commons_annual_change = 0 THEN commons_annual_signal
                        ELSE commons_annual_change*commons_annual_signal END AS commons_editorship,
                    CASE WHEN mediawiki_annual_change = 0 THEN mediawiki_annual_signal
                        ELSE mediawiki_annual_change*mediawiki_annual_signal END AS mediawiki_editorship,
                    CASE WHEN wikipedia_annual_change = 0 THEN wikipedia_annual_signal
                        ELSE wikipedia_annual_change*wikipedia_annual_signal END AS wikipedia_editorship,
                    CASE WHEN wikidata_annual_change = 0 THEN wikidata_annual_signal
                        ELSE wikidata_annual_change*wikidata_annual_signal END AS wikidata_editorship,
                    CASE WHEN wikisource_annual_change = 0 THEN wikisource_annual_signal
                        ELSE wikisource_annual_change*wikisource_annual_signal END AS wikisource_editorship,
                    percent_active_editors
                FROM
                    editorship_input_metrics_hstack),

            project_editorship_inputs AS (
                SELECT
                    geography_level,
                    country_code,
                    STACK(6,
                        commons_editorship, 'commons_editorship',
                        mediawiki_editorship, 'mediawiki_editorship',
                        wikipedia_editorship, 'wikipedia_editorship',
                        wikidata_editorship, 'wikidata_editorship',
                        wikisource_editorship, 'wikisource_editorship',
                        percent_active_editors, 'percent_active_editors') AS (value, metric)
                FROM
                    project_editorship_values),

            project_editorship_input_ranks AS (
                SELECT
                    geography_level,
                    country_code,
                    metric,
                    ROUND(
                        PERCENT_RANK() OVER(
                            PARTITION BY geography_level, metric
                            ORDER BY value), 4) AS p_rank
                FROM
                    project_editorship_inputs),

            editorship_inputs_average AS (
                SELECT
                    geography_level,
                    country_code,
                    AVG(p_rank) AS avg_input_ranks
                FROM
                    project_editorship_input_ranks
                GROUP BY
                    geography_level,
                    country_code),

            editorship AS (
                SELECT
                    geography_level,
                    country_code,
                    ROUND(
                        PERCENT_RANK() OVER (
                            PARTITION BY geography_level
                            ORDER BY avg_input_ranks)*10 , 2) AS metric_value,
                    'editorship' AS metric
                FROM editorship_inputs_average)

        SELECT * FROM presence
        UNION ALL
        SELECT * FROM growth
        UNION ALL
        SELECT * FROM editorship
    """
    ).createOrReplaceTempView("editorship_output_ranks")

def _programs_and_events_metrics(spark):
    spark.sql(
        """
        WITH
            programs_events_input_metrics_hstack AS (
                SELECT
                    geography_level,
                    country_code_iso_2 AS country_code,
                    CAST(COALESCE(avg_monthly_organizers, 0) AS FLOAT) AS avg_monthly_organizers,
                    CAST(COALESCE(avg_monthly_participants, 0) AS FLOAT) AS avg_monthly_participants,
                    CAST(COALESCE(non_affiliate_apg_grants_usd_total, 0) AS FLOAT) AS non_affiliate_apg_grants_usd_total,
                    CAST(COALESCE(non_affiliate_apg_grants_usd_percent, 0) AS FLOAT) AS non_affiliate_apg_grants_usd_percent,
                    CAST(COALESCE(non_apg_grants_count, 0) AS FLOAT) AS non_apg_grants_count
                FROM
                     input_metrics),
            programs_events_input_metrics_vstack AS (
                SELECT
                    geography_level,
                    country_code,
                    STACK(5,
                        avg_monthly_organizers, 'avg_monthly_organizers',
                        avg_monthly_participants, 'avg_monthly_participants',
                        non_affiliate_apg_grants_usd_total, 'non_affiliate_apg_grants_usd_total',
                        non_affiliate_apg_grants_usd_percent, 'non_affiliate_apg_grants_usd_percent',
                        non_apg_grants_count, 'non_apg_grants_count') AS (value, metric)
                FROM
                    programs_events_input_metrics_hstack),
            programs_events_input_ranks AS (
                   SELECT
                        geography_level,
                        country_code,
                        metric,
                        ROUND(
                            PERCENT_RANK() OVER(
                                PARTITION BY geography_level, metric
                                ORDER BY value), 3) AS p_rank
                    FROM
                        programs_events_input_metrics_vstack),
            final_programs_events_input_ranks AS (
              SELECT geography_level,
                     country_code,
                     ROUND(
                        PERCENT_RANK() OVER( PARTITION BY geography_level ORDER BY AVG(p_rank))*10
                        ,2) as metric_value,
                     'programs_leadership' as metric
                 from programs_events_input_ranks
                WHERE metric IN ('avg_monthly_organizers', 'non_apg_grants_count', 'non_affiliate_apg_grants_usd_percent', 'avg_monthly_participants')
               GROUP BY geography_level,country_code
            )
        SELECT *
          FROM final_programs_events_input_ranks
        """
    ).createOrReplaceTempView("programs_and_events_output_ranks")


def _grants_metrics(spark):
    spark.sql(
        """
            WITH
                grants_input_metric_hstack AS (
                    SELECT
                        geography_level,
                        country_code_iso_2 AS country_code,
                        CAST(COALESCE(sum_historical_grants_to_date, 0) AS FLOAT) AS sum_historical_grants_to_date,
                        CAST(COALESCE(sum_calendar_year_grants, 0) AS FLOAT) AS sum_calendar_year_grants,
                        CAST(COALESCE(count_historical_grants_to_date, 0) AS FLOAT) AS count_historical_grants_to_date,
                        CAST(COALESCE(count_calendar_year_grants, 0) AS FLOAT) AS count_calendar_year_grants,
                        CAST(COALESCE(annual_grants_weighted, 0) AS FLOAT) AS annual_grants_weighted,
                        CAST(COALESCE(annual_affiliate_grants_weighted, 0) AS FLOAT) AS annual_affiliate_grants_weighted,
                        CAST(COALESCE(annual_grants_change_rate, 0) AS FLOAT) AS annual_grants_change_rate,
                        CAST(COALESCE(annual_grants_by_annual_change_rate, 0) AS FLOAT) AS annual_grants_by_annual_change_rate,
                        CAST(COALESCE(historical_grants_by_annual_change_rate, 0) AS FLOAT) AS historical_grants_by_annual_change_rate
                    FROM
                        input_metrics),

                grants_input_metric_vstack AS (
                    SELECT
                        geography_level,
                        country_code,
                        STACK(10,
                                sum_historical_grants_to_date, 'sum_historical_grants_to_date',
                                sum_calendar_year_grants, 'sum_calendar_year_grants',
                                count_historical_grants_to_date, 'count_historical_grants_to_date',
                                count_calendar_year_grants, 'count_calendar_year_grants',
                                annual_grants_weighted, 'annual_grants_weighted',
                                annual_affiliate_grants_weighted, 'annual_affiliate_grants_weighted',
                                annual_grants_change_rate, 'annual_grants_change_rate',
                                annual_grants_by_annual_change_rate, 'annual_grants_by_annual_change_rate',
                                historical_grants_by_annual_change_rate, 'historical_grants_by_annual_change_rate') AS (metric_value, metric)
                    FROM
                        grants_input_metric_hstack),

                grants_input_metrics_ranks AS (
                    SELECT
                        geography_level,
                        country_code,
                        metric,
                        PERCENT_RANK() OVER (
                            PARTITION BY geography_level, metric
                            ORDER BY metric_value) AS p_rank
                    FROM
                        grants_input_metric_vstack),

                grants_count_rank_avg AS (
                    SELECT
                        geography_level,
                        country_code,
                        'presence_count_rank_average' AS metric,
                        AVG(p_rank) AS p_rank
                    FROM
                        grants_input_metrics_ranks
                    WHERE
                        metric IN ('count_historical_grants_to_date', 'count_calendar_year_grants')
                    GROUP BY
                        geography_level,
                        country_code),

                presence_rank_input AS (
                    SELECT
                        *
                    FROM
                        grants_input_metrics_ranks
                    WHERE
                        metric IN ('annual_grants_weighted', 'annual_affiliate_grants_weighted')
                    UNION ALL
                    SELECT
                        *
                    FROM
                        grants_count_rank_avg),

                presence AS (
                    SELECT
                        geography_level,
                        country_code,
                        ROUND(
                            PERCENT_RANK() OVER (
                                PARTITION BY geography_level
                                ORDER BY AVG(p_rank))*10, 2) AS metric_value,
                        'grants_presence' AS metric
                    FROM
                        presence_rank_input
                    GROUP BY
                        geography_level,
                        country_code),

                growth AS (
                    SELECT
                        geography_level,
                        country_code,
                        ROUND(
                            PERCENT_RANK() OVER (
                                PARTITION BY geography_level
                                ORDER BY AVG(p_rank))*10, 2) AS metric_value,
                        'grants_growth' AS metric
                    FROM
                        grants_input_metrics_ranks
                    WHERE
                        metric IN ('annual_grants_change_rate')
                    GROUP BY
                        geography_level,
                        country_code),

                leadership_rank_input AS (
                    SELECT
                        *
                    FROM
                        grants_input_metrics_ranks
                    WHERE
                        metric IN ('annual_grants_by_annual_change_rate', 'historical_grants_by_annual_change_rate')
                    UNION ALL
                    SELECT
                        *
                    FROM
                        grants_count_rank_avg),

                leadership AS (
                    SELECT
                        geography_level,
                        country_code,
                        ROUND(
                            PERCENT_RANK() OVER (
                                PARTITION BY geography_level
                                ORDER BY AVG(p_rank))*10, 2) AS metric_value,
                        'grants_leadership' AS metric
                    FROM
                        leadership_rank_input
                    GROUP BY
                        geography_level,
                        country_code)


            SELECT * FROM presence
            UNION ALL
            SELECT * FROM growth
            UNION ALL
            SELECT * FROM leadership
    """
    ).createOrReplaceTempView("grants_output_ranks")


def _affiliate_metrics(spark):
    spark.sql(
        """
        WITH
            affiliates_input_metrics_hstack AS (
                SELECT
                    geography_level,
                    country_code_iso_2 AS country_code,
                    CAST(COALESCE(count_operating_affiliates, 0) AS FLOAT) AS count_operating_affiliates,
                    CAST(COALESCE(affiliate_size_max, 0) AS FLOAT) AS affiliate_size_max,
                    CAST(COALESCE(affiliate_tenure_max, 0) AS FLOAT) AS affiliate_tenure_max,
                    CAST(COALESCE(affiliate_size_growth, 0) AS FLOAT) AS affiliate_size_growth,
                    CAST(COALESCE(affiliate_percent_annual_grants, 0) AS FLOAT) AS affiliate_percent_annual_grants,
                    CAST(COALESCE(affiliate_percent_historical_grants, 0) AS FLOAT) AS affiliate_percent_historical_grants,
                    CAST(COALESCE(historical_affiliate_grants_weighted, 0) AS FLOAT) AS historical_affiliate_grants_weighted,
                    CAST(COALESCE(affiliate_governance_type, 0) AS FLOAT) AS affiliate_governance_type,
                    CAST(COALESCE(affiliate_tenure_average, 0) AS FLOAT) AS affiliate_tenure_average
                FROM
                    input_metrics),
            affiliates_input_metrics_vstack AS (
                SELECT
                    geography_level,
                    country_code,
                    STACK(9,
                        count_operating_affiliates, 'count_operating_affiliates',
                        affiliate_size_max, 'affiliate_size_max',
                        affiliate_tenure_max, 'affiliate_tenure_max',
                        affiliate_percent_annual_grants, 'affiliate_percent_annual_grants',
                        affiliate_percent_historical_grants, 'affiliate_percent_historical_grants',
                        historical_affiliate_grants_weighted, 'historical_affiliate_grants_weighted',
                        affiliate_size_growth, 'affiliate_size_growth',
                        affiliate_tenure_average, 'affiliate_tenure_average',
                        affiliate_governance_type, 'affiliate_governance_type') AS (metric_value, metric)
                FROM
                    affiliates_input_metrics_hstack),
            affiliate_input_metrics_ranks AS (
                SELECT
                    geography_level,
                    country_code,
                    metric,
                    ROUND(
                    PERCENT_RANK() OVER (
                        PARTITION BY geography_level, metric
                        ORDER BY metric_value), 3) AS p_rank
                FROM
                    affiliates_input_metrics_vstack),
            presence AS (
                SELECT
                    geography_level,
                    country_code,
                    ROUND(
                        PERCENT_RANK() OVER (
                            PARTITION BY geography_level
                            ORDER BY AVG(p_rank))*10, 2) AS metric_value,
                    'affiliate_presence' AS metric
                FROM
                    affiliate_input_metrics_ranks
                WHERE
                    metric IN ('count_operating_affiliates', 'affiliate_size_max', 'affiliate_tenure_max', 'affiliate_tenure_average')
                GROUP BY geography_level, country_code),
            growth AS (
                SELECT
                    geography_level,
                    country_code,
                    ROUND(
                        PERCENT_RANK() OVER (
                            PARTITION BY geography_level
                            ORDER BY AVG(p_rank))*10, 2) AS metric_value,
                    'affiliate_growth' AS metric
                FROM
                    affiliate_input_metrics_ranks
                WHERE
                    metric IN ('affiliate_percent_annual_grants', 'affiliate_percent_historical_grants', 'affiliate_size_growth')
                GROUP BY geography_level, country_code),
            leadership AS (
                SELECT
                    geography_level,
                    country_code,
                    ROUND(
                        PERCENT_RANK() OVER (
                            PARTITION BY geography_level
                            ORDER BY AVG(p_rank))*10, 2) AS metric_value,
                    'affiliates_leadership' AS metric
                FROM
                    affiliate_input_metrics_ranks
                WHERE
                    metric IN ('count_operating_affiliates', 'affiliate_size_max', 'affiliate_tenure_max',
                                'affiliate_governance_type', 'historical_affiliate_grants_weighted')
                GROUP BY geography_level, country_code)
        SELECT * FROM presence
        UNION ALL
        SELECT * FROM growth
        UNION ALL
        SELECT * FROM leadership
        """
    ).createOrReplaceTempView("affiliates_output_ranks")


def _overall_engagement_metrics(spark):
    spark.sql(
        """
    WITH
        inputs AS (
            SELECT * FROM readership_output_ranks WHERE metric = 'readership'
            UNION ALL
            SELECT * FROM editorship_output_ranks WHERE metric = 'editorship'
            UNION ALL
            SELECT * FROM affiliates_output_ranks WHERE metric = 'affiliates_leadership'
            UNION ALL
            SELECT * FROM grants_output_ranks WHERE metric = 'grants_leadership')
    SELECT
        geography_level,
        country_code,
        ROUND(
            PERCENT_RANK() OVER (
                PARTITION BY geography_level
                ORDER BY AVG(metric_value))*10, 2) AS metric_value,
        'overall_engagement' AS metric
    FROM
        inputs
    GROUP BY geography_level, country_code
    """
    ).createOrReplaceTempView("overall_engagement_output_rank")


def _access_metrics(spark):
    spark.sql(
        """
        WITH
            access_inputs_hstack AS (
                SELECT
                    geography_level,
                    country_code_iso_2 AS country_code,
                    COALESCE(internet_percent_annual_signal, 0) AS internet_percent_annual_signal,
                    COALESCE(mobile_subscriptions_annual_signal, 0) AS mobile_subscriptions_annual_signal,
                    COALESCE(access_to_basic_knowledge_annual_signal, 0) AS access_to_basic_knowledge_annual_signal,
                    COALESCE(annual_connectivity_index_annual_change, 0) AS annual_connectivity_index_annual_change,
                    COALESCE(internet_annual_change, 0) AS internet_annual_change
                FROM
                    input_metrics),
            access_inputs_vstack AS (
                SELECT
                    geography_level,
                    country_code,
                    STACK(5,
                            internet_percent_annual_signal, 'internet_percent_annual_signal',
                            mobile_subscriptions_annual_signal, 'mobile_subscriptions_annual_signal',
                            access_to_basic_knowledge_annual_signal, 'access_to_basic_knowledge_annual_signal',
                            annual_connectivity_index_annual_change, 'annual_connectivity_index_annual_change',
                            internet_annual_change, 'internet_annual_change')
                                AS (metric_value, metric)
                FROM
                    access_inputs_hstack),
            access_input_ranks AS (
                SELECT
                    geography_level,
                    country_code,
                    metric,
                    PERCENT_RANK() OVER(
                        PARTITION BY geography_level, metric
                        ORDER BY metric_value) AS p_rank
                FROM
                    access_inputs_vstack),
            presence AS (
                SELECT
                    geography_level,
                    country_code,
                    ROUND(
                        PERCENT_RANK() OVER (
                            PARTITION BY geography_level
                            ORDER BY AVG(p_rank))*10, 2) AS metric_value,
                    'access_presence' AS metric,
                    AVG(p_rank) AS avg_presence_rank
                FROM
                    access_input_ranks
                WHERE
                    metric IN ('internet_percent_annual_signal', 'mobile_subscriptions_annual_signal',
                                'access_to_basic_knowledge_annual_signal', 'access_access_to_information')
                GROUP BY geography_level, country_code),
            growth AS (
                SELECT
                    geography_level,
                    country_code,
                    ROUND(
                        PERCENT_RANK() OVER (
                            PARTITION BY geography_level
                            ORDER BY AVG(p_rank))*10, 2) AS metric_value,
                    'access_growth' AS metric
                FROM
                    access_input_ranks
                WHERE
                    metric IN ('internet_annual_change', 'annual_connectivity_index_annual_change')
                GROUP BY geography_level, country_code),
            access_pg_input AS (
                SELECT
                    ip.geography_level,
                    ip.country_code,
                    avg_growth*avg_presence_rank*10 AS average_rate
                FROM (
                    SELECT
                        geography_level,
                        country_code,
                        AVG(metric_value) AS avg_growth
                    FROM
                        access_inputs_vstack
                    WHERE
                        metric IN ('internet_annual_change', 'annual_connectivity_index_annual_change')
                    GROUP BY
                        geography_level, country_code) ip
                JOIN
                    presence p
                    ON ip.geography_level = p.geography_level AND ip.country_code = p.country_code),
            access_presence_growth AS (
                SELECT
                    geography_level,
                    country_code,
                    ROUND(
                        PERCENT_RANK() OVER (
                            PARTITION BY geography_level
                            ORDER BY average_rate)*10, 2) AS metric_value,
                    'access' AS metric
                FROM
                    access_pg_input)
        SELECT
            geography_level,
            country_code,
            metric_value,
            metric
        FROM
            presence
        UNION ALL
        SELECT * FROM growth
        UNION ALL
        SELECT * FROM access_presence_growth
    """
    ).createOrReplaceTempView("access_output_ranks")


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--year", type=str, help="Year for which metrics should be calculated for"
    )
    parser.add_argument("--output-path", type=str, help="Path to write output to")

    parser.add_argument("--input-path", type=str, help="Path to read input from")

    args = parser.parse_args()

    if args.year:
        year = int(args.year)
    else:
        raise ValueError("Year not specified")

    if args.output_path:
        output_path = args.output_path
    else:
        raise ValueError("Output path not specified")

    if args.input_path:
        input_path = args.input_path
    else:
        raise ValueError("Input path not specified")

    input_path = f"{input_path}/year={year}"
    output_path = f"{output_path}/year={year}"

    main(year=year, output_path=output_path, input_path=input_path)
