import argparse

from pyspark import SparkConf
from pyspark.sql import SparkSession

from gdi_source.util import filter_out_metrics, write_df_to_json

"""
This module contains functions to generate dashboard output for a given year.


Functions:
    _process_args() -> argparse.Namespace:
        Parses command line arguments and returns an argparse.Namespace object.
    main(output_metrics_path: str, regional_metrics_path: str, output_path: str) -> None:
        The main function to execute the Spark job that generates dashboard output for a given year.
"""

def _process_args()-> argparse.Namespace:
    """Processes command line arguments year: int, input_path: str, output_path: str"""
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--output-metrics-path",
        help="HDFS location for output metrics parquet files to be read",
        required=True,
    )

    parser.add_argument(
        "--regional-metrics-path",
        help="HDFS location for regional metrics parquet files to be read",
        required=True,
    )

    parser.add_argument(
        "--output-path",
        help="HDFS location for JSON files to be written",
        required=True,
    )

    args = parser.parse_args()

    return args


def main(
    output_metrics_path: str, regional_metrics_path: str, output_path: str
) -> None:
    """
    The main function to execute the Spark job that generates dashboard output for a given year.

    This function reads the input data from the specified input path, filters out blocklisted data,
    creates a geographic hierarchy, and writes the resulting JSON output to the specified output path.

    Args:
        year (int): The year for which the dashboard output should be generated.
        output_metrics_path (str): The HDFS path to the output metrics parquet files.
        regional_metrics_path (str): The HDFS path to the regional metrics parquet files.
        output_path (str): The HDFS path to the output JSON files.
    """

    conf = SparkConf().setAppName("equity_landscape_dashboard_output")
    spark = SparkSession.builder.config(conf=conf).getOrCreate()

    # Read the output metrics
    df_output_metrics = spark.read.parquet(output_metrics_path)

    # Read the regional metrics
    df_regional_metrics = spark.read.parquet(regional_metrics_path)

    columns_to_select = [
        "geography_level",
        "country_name",
        "sub_continent",
        "country_code_iso_2",
        "continent",
        "affiliate_size_max",
        "count_operating_affiliates",
        "affiliate_tenure_max",
        "year",
    ]

    df_regional_metrics = df_regional_metrics.select(columns_to_select).fillna(0)

    df_blacklist = spark.sql("SELECT * FROM canonical_data.countries WHERE is_protected = true")

    lst_blacklist_countries = (
        df_blacklist.selectExpr("iso_code").rdd.flatMap(lambda x: x).collect()
    )

    lst_exclude_metrics = ["overall_engagement"]

    df_output_metrics = filter_out_metrics(
        df=df_output_metrics,
        join_column="country_code",
        lst_blacklist_countries=lst_blacklist_countries,
        lst_exclude_metrics=lst_exclude_metrics,
    )

    df_regional_metrics = filter_out_metrics(
        df=df_regional_metrics,
        join_column="country_code_iso_2",
        lst_blacklist_countries=lst_blacklist_countries,
        lst_exclude_metrics=lst_exclude_metrics,
    )

    df_countries = spark.sql("SELECT canonical_country_name as country_name, country_code_iso_2, country_code_iso_3, continent_name, subcontinent_name from gdi.country_meta_data")

    # Write the output metrics to a JSON file
    write_df_to_json(
        df=df_output_metrics, filename=f"{output_path}/output_metrics.json"
    )

    # Write the regional metrics to a JSON file
    write_df_to_json(
        df=df_regional_metrics, filename=f"{output_path}/regional_metrics.json"
    )

    # Write the country metadata to a JSON file
    write_df_to_json(
        df=df_countries, filename=f"{output_path}/country_data.json"
    )

    spark.stop()


if __name__ == "__main__":
    args = _process_args()

    main(
        args.output_metrics_path,
        args.regional_metrics_path,
        args.output_path,
    )
