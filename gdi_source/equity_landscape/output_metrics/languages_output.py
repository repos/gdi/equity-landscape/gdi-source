import argparse

from pyspark import SparkConf
from pyspark.sql import DataFrame, SparkSession

from gdi_source.util import create_select, write_df_to_json

"""
This module contains functions to generate language dashboard output for a given year.

Functions:
    _process_args() -> argparse.Namespace:
        Parses command line arguments and returns an argparse.Namespace object.
    brief_projects_edited(schema: str, spark: SparkSession) -> DataFrame:
        Returns a DataFrame containing the brief projects edited metrics.
    languange_metrics(schema: str, spark: SparkSession) -> DataFrame:
        Returns a DataFrame containing the language metrics.
    unesco_endangered_languages(schema: str, spark: SparkSession) -> DataFrame:
        Returns a DataFrame containing the UNESCO endangered languages metrics.
    main(schema: str, output_path: str) -> None:
        The main function to execute the Spark job that generates language dashboard output for a given year.
"""


def _brief_projects_edited(schema: str, spark: SparkSession) -> DataFrame:
    sql = f"""
        SELECT brief.proportion_editors      AS proportion,
               brief.average_active_editors  AS average_active_editors,
               brief.list_label              AS project_name,
               brief.language_name,
               cmd.canonical_country_name    AS country_name,
               brief.year
         FROM {schema}.brief_projects_edited_metrics brief,
              {schema}.country_meta_data cmd
        WHERE (average_active_editors >=10)
          AND brief.iso2_country_code = cmd.country_code_iso_2
          AND brief.year >= 1999
    """

    return spark.sql(sql)


def _languange_metrics(schema: str, spark: SparkSession) -> DataFrame:
    sql = f"""
    SELECT lang.language_name              AS language,
           lang.language_status            AS status,
           cmd.canonical_country_name AS country_name
      FROM {schema}.official_language_metrics lang,
           {schema}.country_meta_data cmd
      WHERE lang.iban2_country_code = cmd.country_code_iso_2
    """

    return spark.sql(sql)


def _unesco_endangered_languages(schema: str, spark: SparkSession) -> DataFrame:

    select_columns = create_select(
        [
            "language_name_english",
            "degree_of_endangerment",
            "number_of_speakers",
            "year",
        ]
    )

    sql = f"""
     WITH languages AS (
         SELECT {select_columns}, explode(country_codes) as country_code
           FROM {schema}.unesco_endangered_lang_metrics
          WHERE year > 1999
     )
     SELECT *
       FROM (
     SELECT distinct lang.language_name_english     AS language,
                     lang.degree_of_endangerment,
                     lang.number_of_speakers,
                     cmd.canonical_country_name     AS country_name
        FROM languages lang,
             {schema}.country_meta_data cmd
        WHERE trim(lang.country_code) = cmd.country_code_iso_3
      ) t
    """

    return spark.sql(sql)


def main(schema: str, output_path: str) -> None:
    """
    This script is used to generate the output metrics for the languages section of the equity landscape.

    input: schema

    """

    conf = SparkConf()
    conf.setAppName("equity_landscape_language_output")
    spark = SparkSession.builder.config(conf=conf).getOrCreate()

    df_language_metrics = _languange_metrics(schema=schema, spark=spark)

    df_unesco_endangered_languages = _unesco_endangered_languages(
        schema=schema, spark=spark
    )

    df_brief_projects_edited = _brief_projects_edited(schema=schema, spark=spark)

    write_df_to_json(
        df=df_language_metrics, filename=f"{output_path}/language_metrics.json"
    )

    write_df_to_json(
        df=df_unesco_endangered_languages,
        filename=f"{output_path}/unesco_endangered_languages.json",
    )

    write_df_to_json(
        df=df_brief_projects_edited,
        filename=f"{output_path}/brief_projects_edited.json",
    )


def _process_args():
    parser = argparse.ArgumentParser()

    parser.add_argument("--schema", type=str, help="Schema to read from", required=True)

    parser.add_argument(
        "--output-path", type=str, help="Path to write output to", required=True
    )

    return parser.parse_args()

if __name__ == "__main__":

    args = _process_args()

    main(schema=args.schema, output_path=args.output_path)
