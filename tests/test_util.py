import pytest
from pyspark.sql import SparkSession
from pyspark.sql.types import StructType, StructField, StringType, IntegerType
from gdi_source.util import (
    create_select,
    df_to_list_of_dicts,
    drop_partition_columns,
    fetch_lua_data,
    fetch_world_bank_data,
    get_columns,
    get_partition_statement,
    get_schema,
    handle_true_false,
    filter_out_metrics,
    is_valid_sql_name,
    remove_partition_column_from_schema,
    rename_columns,
    set_proxy,

)

@pytest.fixture(scope="module")
def spark():
    spark = SparkSession.builder.appName("test").getOrCreate()
    yield spark
    spark.stop()


@pytest.fixture(scope="module")
def test_df(spark):
    schema = StructType([
        StructField("name", StringType(), True),
        StructField("age", IntegerType(), True),
        StructField("gender", StringType(), True),
        StructField("address", StringType(), True)
    ])
    data = [("Alice", 25, "F", "Wiki Street"), ("Bob", 30, "M", "Media Street")]
    return spark.createDataFrame(data, schema)

def test_handle_true_false():
    assert handle_true_false("test_field", "1") == True
    assert handle_true_false("test_field", "0") == False
    with pytest.raises(ValueError):
        handle_true_false("test_field", "2")

def test_filter_out_metrics(spark):
    df = spark.createDataFrame(
        [
            ("country1", "metric1", 10),
            ("country1", "metric2", 20),
            ("country2", "metric1", 30),
            ("country2", "metric2", 40),
        ],
        ["country", "metric", "value"],
    )
    blacklist = ["country1"]
    exclude_metrics = ["metric1"]
    result = filter_out_metrics(df, "country", blacklist, exclude_metrics)
    assert result.count() == 1
    assert result.first()["metric"] == "metric2"
    assert result.first()["value"] == 40

def test_set_proxy():
    assert set_proxy({"http": "http://proxy.com"}) is None

def test_set_proxy_empty_dict():
    with pytest.raises(Exception):
        set_proxy({})

def test_rename_columns(spark):
    df = spark.createDataFrame([(1, "foo"), (2, "bar")], ["id", "name"])
    schema = StructType([StructField("value", IntegerType(), True), StructField("name", IntegerType(), True)])

    assert rename_columns(df, schema).schema == spark.createDataFrame([(1, "foo"), (2, "bar")], ["value", "name"]).schema

def test_rename_columns_empty_df(spark):
    df = spark.createDataFrame([], StructType([]))
    schema = StructType([StructField("value", IntegerType(), True), StructField("name", IntegerType(), True)])
    with pytest.raises(ValueError):
        rename_columns(df, schema)

def test_rename_columns_none_schema(spark):
    df = spark.createDataFrame([(1, "foo"), (2, "bar")], ["id", "name"])
    schema = None
    with pytest.raises(ValueError):
        rename_columns(df, schema)

def test_rename_columns_empty_schema(spark):
    df = spark.createDataFrame([(1, "foo"), (2, "bar")], ["id", "name"])
    schema = StructType([])
    with pytest.raises(ValueError):
        rename_columns(df, schema)

def test_get_columns(test_df):
    assert get_columns(test_df) == ["name", "age", "gender", "address"]
    assert get_columns(test_df, ["age", "address"]) == ["name", "gender"]
    assert get_columns(test_df, ["nonexistent_column"]) == ["name", "age", "gender", "address"]
    with pytest.raises(ValueError):
        get_columns(None)
    assert get_columns(test_df, []) == ["name", "age", "gender", "address"]

def test_get_schema(spark):
    table_name = "test_table"
    data = [(1, "foo"), (2, "bar")]
    df = spark.createDataFrame(data, ["id", "name"])
    df.createOrReplaceTempView(table_name)
    expected_output = df.schema
    assert get_schema(spark, table_name) == expected_output

def test_drop_partition_columns(spark):
    data = [(1, "foo", "2021", "01"), (2, "bar", "2021", "02")]
    df = spark.createDataFrame(data, ["id", "name", "year", "month"])
    partition_columns = "year='2021', month='01'"
    expected_output = spark.createDataFrame([(1, "foo"), (2, "bar")], ["id", "name"])
    assert drop_partition_columns(df, partition_columns).collect() == expected_output.collect()

def test_remove_partition_column_from_schema(spark):
    data = [(1, "foo", "2021", "01"), (2, "bar", "2021", "02")]
    df = spark.createDataFrame(data, ["id", "name", "year", "month"])
    partition_columns = "year='2021', month='01'"
    schema = df.schema
    expected_output = spark.createDataFrame([(1, "foo"), (2, "bar")], ["id", "name"]).schema
    assert remove_partition_column_from_schema(schema, partition_columns) == expected_output

def test_filter_out_metrics(spark):
    data = [("2021-01-01", "US", 5, 100), ("2021-01-01", "US", 2, 100), ("2021-01-01", "UK",50, 50), ("2021-01-01", "UK", 30, 500)]
    df = spark.createDataFrame(data, ["date", "country", "metricA", ""])
    join_column = "country"
    lst_blacklist_countries = ["US"]
    lst_exclude_metrics = ["metricA"]
    expected_data = [("2021-01-01", "US", 5, 0), ("2021-01-01", "US", 2, 0) ,("2021-01-01", "UK", 50, 50), ("2021-01-01", "UK", 30, 500)]
    expected_output = spark.createDataFrame(expected_data, ["date", "country", "metricA", "metricB"])
    assert filter_out_metrics(df, join_column, lst_blacklist_countries, lst_exclude_metrics).collect() == expected_output.collect()

def test_df_to_list_of_dicts(spark):
    data = [(1, "foo"), (2, "bar")]
    df = spark.createDataFrame(data, ["id", "name"])
    expected_output = [{"id": 1, "name": "foo"}, {"id": 2, "name": "bar"}]
    assert df_to_list_of_dicts(df) == expected_output

def test_fetch_data():
    assert next(fetch_world_bank_data(2, "IT.NET.USER.ZS"),None) is not None

def test_fetch_data_empty_1():
    with pytest.raises(ValueError):
        next(fetch_world_bank_data(2, ""),None)

def test_fetch_data_empty_2():
    with pytest.raises(ValueError):
        next(fetch_world_bank_data(None, "IT.NET.USER.ZS"),None)

def test_fetch_data_empty_3():
    with pytest.raises(ValueError):
        next(fetch_world_bank_data(None, None),None)

def test_fetch_data_empty_4():
    with pytest.raises(ValueError):
        next(fetch_world_bank_data(2, None),None)

def test_fetch_data_invalid():
    with pytest.raises(ValueError):
        next(fetch_world_bank_data(2, "invalid"),None)

def test_fetch_lua_data():
    assert len(fetch_lua_data(page_name="Module:Organizational_Informations")) > 0

def fetch_lua_data_no_page_name():
    with pytest.raises(ValueError):
        fetch_lua_data("")

def fetch_lua_data_none_name():
    page_name = None
    with pytest.raises(ValueError):
        fetch_lua_data(page_name)

def test_get_partition_statement():
    assert (
        get_partition_statement(partition_columns="year='2021'")
        == "PARTITION (year='2021')"
    )


def test_get_partition_statement_multiple():
    assert (
        get_partition_statement(partition_columns="year='2021',month='01'")
        == "PARTITION (year='2021',month='01')"
    )


def test_get_partition_statement_empty():
    assert get_partition_statement(partition_columns="") == ""


def test_remove_partition_column_from_schema():
    assert remove_partition_column_from_schema(
        schema=StructType(
            [
                StructField("a", IntegerType(), True),
                StructField("b", IntegerType(), True),
            ]
        ),
        partition_columns="a",
    ) == StructType([StructField("b", IntegerType(), True)])


def test_remove_partition_column_from_schema_multiple():
    assert remove_partition_column_from_schema(
        schema=StructType(
            [
                StructField("a", IntegerType(), True),
                StructField("b", IntegerType(), True),
            ]
        ),
        partition_columns="a,b",
    ) == StructType([])


def test_remove_partition_column_from_schema_empty():
    assert remove_partition_column_from_schema(
        schema=StructType(
            [
                StructField("a", IntegerType(), True),
                StructField("b", IntegerType(), True),
            ]
        ),
        partition_columns="",
    ) == StructType(
        [StructField("a", IntegerType(), True), StructField("b", IntegerType(), True)]
    )

def test_create_select():
    column_list = ["a", "b", "c"]
    assert create_select(column_list) == "a,b,c"

def test_create_select_empty():
    column_list = []
    assert create_select(column_list) == ""

def test_create_with_prefix():
    column_list = ["a", "b", "c"]
    prefix = "test"
    assert create_select(column_list, prefix) == "test.a,test.b,test.c"

def test_is_valid_sql_name():
    assert is_valid_sql_name("a")

def test_is_valid_sql_name_empty():
    assert not is_valid_sql_name("")

def test_is_valid_sql_name_invalid():
    assert not is_valid_sql_name("a b")

def test_is_valid_sql_name_invalid2():
    assert not is_valid_sql_name("a,b")

def test_is_valid_sql_name_invalid3():
    assert not is_valid_sql_name("'a.b'")

def test_is_valid_sql_name_none():
    assert not is_valid_sql_name(None)
